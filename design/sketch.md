# Language sketch

## Key principles

- No designing around bad programmers
- No hidden control flow
- No hidden memory allocations
- No silver bullet solutions to all problems
- No crasy syntax
- No preposterous goals
- Perfomance oriented
- No automatic runtime overhead
- Imperative over functional
- Easy and fast build-test-run cycles
- Tooling is not a luxury and must come out of the box
- Change is inevitable
- Refactoring should be comfortable
- Backward compatibility is important, but not a hill to die on
- Do not recover from programmer error
- Solutions which handle majority of cases are good enough
- Handling errors and nulls must be easier than ignoring them
- Programming may be a complex task, but must not be a complicated one
- Bad code can be written in any language
- Good code can only be encouraged, not forced
- Good programmers make mistakes, ecosystem must help them, but not tie their hands
- Language must not be bloated with features, but programming should not be tedious
- Reduce formatting style debates as much as possible

### Nice features to have (or avoid)

- Compile-time evaluations
- Comfortable usage of low-level features: pointers, memory managment, bit manipulations
- Comfortable usage of concurrency and parallelism
- Syntax with balance of human readability and easy parsing
- Link only reachable code into executable
- Interoperability with C (at least ability to use existing C code and libraries)
- Swappable compiler backends (LLVM, C/C++, WASM, ...)
- Simple borrow-checking (to catch obvious bugs)
- Helpful compiler error messages
- Dangerous places in code should be localized
- Safety debug mode for runtime programmer error and UB detection
- Generics for repetetive abstract code
- Type inference for simple cases
- No arrow dereferencing operator (`ptr.field` instead of `ptr->field`)
- No implicit narrowing (or any) type conversions
- Built-in handy binary serialization of data types
- Easy to understand system for platform-specific code
- Custom compile-time failures (assertions?)
- Noreturn and tuple (with handy multi-variable assignment) types
- Closures (maybe restrict level of nesting for them)
- Defined overflow behaviour and language support to work with it
- SIMD support in standard library
- Warnings about no-op statements, unreachable code, etc.
- Comments to docs system
- Mark pure functions for compiler to check (maybe color, tag or set attribute system?)
- Lamdas are handy, but should not be default solution for everything

### Big maybe features

- Optional GC for some parts of program

### What this project is not after

- Ultimate safety
- Pure functions everything
- All-encompassing type system
- Happy-path only programming
- Chasing paradigm X
- Awesome oneliners

### No-go features

- Inheritance (C++ or Java styles)
- Exceptions (or any implicit stack unwinding)
- Complicated RAII-like rules
- Complex macro system
- Unicode identifiers (only string and character literals and comments are allowed to contain non-ASCII)
- Non-trivial move or copy semantics

## Ideas and examples

### Semicolons

Semicolons litter code. But they are a good tradeoff between ease of statement parsing
and human readability. Making them optional will only lead to meaningless debates and
more work for automated formatting. Thus semicolons are required at the end of statements
that end with expression. Loops, branching and top-level definitions do not need semicolons, they are easy to parse by itself

### Short assignment

Naming new things as you create them should be easy. Language should encourage usage of
constants over variables. Thus shortest form of declaration creates constants. Type is
inferred if omitted

```ku
// short assignment creates a constant, type is inferred
a := foo(10);
```

### Const declarations

Declaring a constant should be a go-to decision for programmer. Thus keywords for declaring
constants should be avoided. However changes between constants and variables should
be easy to make

```ku
// declare a constant with specific type
a : i32 = 10;

// short assignment creates a constant with inferred type
b := 10;

// declaring a constant with no value is an error
// uncomment statemnt below to see compiler error
// c : u64;
```

### Variable declarations

```ku
// declare a variable with default initialization
// default (zero) value of the type is assigned to variable
var a: i32;

// variable can be left uninitialized explicitly
// by "assigning" dirty value to it (just a syntax, becomes a nop after translation)
var d: i32 = dirty;

// declare and init a variable
var b: i32 = 10;

// declare and init a variable with type inference
// best suited type will be used by compiler for this variable
var c := 10;

// consider difference between declaring variable and constant with short assignments
n := 10; // n is constant
```

### Function definitions

```ku
// function which returns nothing should not specify that with extra words,
// but such functions cannot be pure
fn do_something() {
    // some code here
}

// if function returns something it must be specified explicitly
fn create_number() => i32 {
    return 10;
}

// arguments are tuples
fn add(a: i32, b: i32) => i32 {
    return a + b;
}

// return values are also tuples
fn swap(a: i32, b: i32) => (i32, i32) {
    return b, a;
}

// named fields can be used for return tuples
fn find_string() => (v: str, ok: bool) {
    return "hello", true;
}

// returned tuple can be used as a value
t := find_string();
if t.ok {
    // field access for tuples can only be used
    // with named fields
    print(t.v);
}

// or can be destructured in-place
v, ok := find_string();
if ok {
    print(v);
}
```

### Mark noreturn functions

```ku
fn run_event_loop() => never {
    for {
        // handle events
    }
}

fn exit() => never {
    // terminate process
}
```

### Define new types

```ku
// no struct keyword needed for defining a type with fields
type ComplexIndex struct {
    i: i32,
    j: u32,
}

type Number union { i32 | f64 | u64 }
```

### Error and optional types

```ku
fn find_by_index(i: i32) => nil|Triangle {
    // some Triangle searching code here

    if not_found {
        return nil;
    }
    return triangle;
}

t := find_by_index(10);
if t == nil {
    print("not found");
}

// t is inferred to be not-nil below
t.render();
```

### Array-like types

```ku
var a: [4]u8; // array of exactly 4 bytes
var b: []u8; // slice = pointer to first element + length
```

### Loops

```ku
// iterate 45 times
for range(45) {
    // some code
}

// iterate with i from 0 to 44 (included)
for i := range(45) {

}

// infinite loop
for {
    // some code
}

// loop with predicate condition
var i := 0;
for i < 10 {
    i += 1;
}
```

### Generic types

```ku
var a: u64 = 54;

// non-nil pointer to u64 number
var b: *u64 = &a;

// possibly nil pointer
var c: ?u64 = nil;

// add $ before parameter to make it compile-time only
type Node($T: any) struct {
    value: T,

    // TODO: think about compile-time argument syntax
    next: ?Node($ T),
}

var node: Node($ u64) = .{
    value: 10,
    next: nil,
}

fn add(a: i32, b: i32) => i32 {
    return a + b;
}

sum := $ add(4, -3);
```

### Composite literals

```ku
// declare a slice
s := []u8.[ 1, 2, 3 ];

// array
a := [_]u8.[ 1, 2, 3 ];

// list literal, can be coerced to slice or array
list := .[ 1, 2, 3 ];
```

### Pointer operations

```ku
var v := 0;
p := v.&; // get address of a local variable
p.@ = 1; // use pointer to assign a value to v

var s := .{
    a: 23,
    b: "foo",
};
p2 := s.a.&; // get address of field in struct
p2.@ = 0;
```

### Methods and dynamic types

```ku
type Email struct {
    subject: str,

    // other fields
}

bind e: *Email {
    fn send(address: str) {
        print(e.subject) // handy debug message

        // code to send email
    }
}

type Reader bag {
    read(buf: []u8) => (usz, error)
}
```

### Imports

Keywords `pub` and `import` are more related to build and dependency system,
then to the language itself. Import block binds exported symbols from other
units to identifiers, local to current unit. Imported unit is specified by a string
which for now is just called "import string". The actual resolution mechanism
which links that string to an actual code of another unit is not a part of
language specification. See topic **build system** for more details

```ku
// imports declared in this block will be visible to other units
pub import {

}

import {
    mem => "std: mem"

    some_3rd_party_module => "mod: other/module"

    local_module => "just/any/directory"
}
```

### Insist and chain

```ku
fn nil_or_five(flag: bool) => nil|i32 {
    if flag {
        return 5;
    }
    return nil;
}

fn use_insist() {
    r := nil_or_five(true); // type nil|i32
    n := r.!; // type i32 and equals 5
}

fn use_chain() {
    n := nil_or_five(false).?; // type i32
    r := n; // type i32, but will not be reached due to nil in previous line
}
```

### Enums

```ku
type Color enum {
    Black,
    Red,
    Green,
    Blue,
    White,
}
```

### Bitsets

```ku
type FileFlags bitset {
    read,
    write,
    append,
    pipe,
}
```

### Struct memory layout

Order of field declarations does not represent memory layout of struct.
Also layout may change based on struct usage. For example a local variable
holding a struct and an array structs may have different layouts in memory
to maximaze efficiency

```ku
type Mesh struct {
    // fields here
}

// gives struct "size" - memory offset difference between two consecutive
// elements in array of such structs
s := Mesh.stride;
```

### (?) Typed pointers idea

Code could document what type of memory regions are allowed in certain places

For example if a function accepts a pointer it may specify that this pointer
should only come from text or static section and thus heap pointer cannot be passed
to this function as an argument. The same goes for struct fields

Maybe lifetimes can be combined with this idea

### Attributes

Attribute system is an instrument to fine tune behaviour of functions, types and
isolated piece of code

```ku
// below is an example of pure function
// pure functions can be explicitly marked for compiler to check it as a contract

# pure
fn add(a: i32, b: i32) => i32 {
    return a + b;
}

// TODO: think about "pure" attribute but with memory allocation
// TODO: attribute for variables which are exposed to concurrent execution
```

### IR type system

Every symbol, struct field, expression and its subexpressions have type
which is determined during IR construction or subsequent type inference
passes over IR tree. This type is a pointer to an instance of special Type
struct. Each type in a program has distinct and unique instance of Type
associated with it

Each Type has a Base. Base is the raw definition of a type in form of
specific struct, enum, union, etc. Base is distinct and unique for every
distinct raw type. Types which has the same Base are called Flavors (always
includes Base itself). Flavors can be cast among each other via explicit type
casts

Some types could be converted into another type with different Base in two cases:

1. Number conversions (u64 -> i32)
2. Pulling a specific type from bag

Implicit type conversions are possible in certain circumstances. Most notable
is reducing ideal type to a concrete type. Primitive literals and constants
without specified type have ideal type of integer, float, string, object or list

### Type checks and casts

```ku
// create a bag and put file into it
var r: io.Reader = file;

// dynamic type check, does not cause panic
if r is io.Reader {
    // do some code
} else {
    // do something else
}

// dynamic type cast, may fail and cause panic
f := r as *fs.File;

// builtin bag type any can hold any type inside
a : any = f;
```

### Prototypes and parameters

```ku
type Arr<T: any, len: usz> [len]T

fn comp_time_add<a: i32, b: i32> => i32 {
    return a + b;
}
```

### Overflow behaviour is part of the type

By default integer types panic on overflows in safe mode and UB on optimized mode.
But that can be changed with type augments

```ku
var i: u8[wrap] = 255;
i += 1;

print(i == 0) // true

// TODO: consider predefined types: u8w, u16w, etc.
```

### Bind methods from other flavors

```ku
bind m: Map {

    // Map is a Flavor of Picture
    use resize = Picture.resize;

    use Atlas.*

    draw() {
        m.resize()
        
        // drawing code
    }
}
```

### Optional explicit interface implementation check

```ku

type FlawlessWriter bag {
    write(p []u8)
}

#[implements: FlawlessWriter]
type Buf struct {
    chunk   []u8
    pos     u32
}

bind b: *Buf {
    fn write(p []u8) {
        // stub implementation
    }
}

```

### Changing types

Different flavors of the same type can be freely converted amongst each other
by explicit `as` operator:

```ku
type MyInt i32;

a: i32 = 14;
var b: MyInt;
b = as<MyInt>(a);
```
