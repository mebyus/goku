# Build

Ku toolchain has a dedicated builder to automate compilation of unit that comprise a package. Builder is
invoked with command

```sh
ku build <unit_path>
```

Roughly algorithm which builder follows consists of these steps:

1. Start (add to queue) from given unit path inside package
2. Lex and parse next unit from queue
3. Lookup imported units
