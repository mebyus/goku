# Bring local build variables into scope
-include .build.env

ifeq (${BUILD}, debug)
# Disable compiler optimizations and inlining for debug build
GCFLAGS = -gcflags="all=-N -l"
else
BUILD = release
endif

.PHONY: default
default: fmt vet test build

.PHONY: build
build:
	go build ${GCFLAGS} -o ku ./cmd

.PHONY: fmt
fmt:
	go fmt ./...

.PHONY: vet
vet:
	go vet ./...

.PHONY: test
test:
	go test ./...
