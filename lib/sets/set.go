package sets

// Set is a collection of things in which each element is unique.
// Zero value of this type is nil and is not safe for usage.
// See New or Empty for constructing a Set with safety guarantees
//
// To construct an empty Set with preallocated space use make function.
// For example to preallocate an empty Set of strings with enough space to hold
// 16 elements one should use:
//
//	s := make(Set[string], 16)
//
// Use len(set) function to check number of elements in Set. Such usage
// includes nil Set
//
// Values of this type behave as pointer types. Assigning them does not
// create a deep (memory) copy. If you do need to create a deep copy of
// a Set use Clone
//
// Values of this type are not safe for concurrent usage
type Set[ElementType comparable] map[ElementType]struct{}

// New creates new Set from slice of elements. If there are duplicates
// in a slice, only one of them will be placed in Set. Created Set
// is ready and safe for usage even if slice contains no elements
func New[ElementType comparable](elements []ElementType) Set[ElementType] {
	set := UnsafeNew(elements)
	if len(set) == 0 {
		return Empty[ElementType]()
	}
	return set
}

// UnsafeNew is unsafe version of New. It returns nil if provided
// slice contains no elements
func UnsafeNew[ElementType comparable](elements []ElementType) Set[ElementType] {
	if len(elements) == 0 {
		return nil
	}
	set := make(Set[ElementType], len(elements))
	for _, element := range elements {
		set.Add(element)
	}
	return set
}

// From (stands for "Literal") is a convenience function mainly for constructing Sets from literals.
// Behaves the same way as New does, but is invoked with multiple arguments
// instead of slice. Created Set is ready and safe for usage even if slice
// contains no elements
func From[ElementType comparable](elements ...ElementType) Set[ElementType] {
	return New(elements)
}

// UnsafeFrom is unsafe version of Lit. It returns nil if invoked
// without arguments
func UnsafeFrom[ElementType comparable](elements ...ElementType) Set[ElementType] {
	return UnsafeNew(elements)
}

// Empty creates empty Set which is ready for usage
func Empty[ElementType comparable]() Set[ElementType] {
	return make(Set[ElementType])
}

// Unique returns new slice containing only unique elements from original slice.
// Returns nil if original slice contains no elements
//
// Keep in mind that this function always returns new slice even if all elements
// in original slice were unique. If such behaviour is undesirable you should
// consider using function Retain
func Unique[ElementType comparable](elements []ElementType) []ElementType {
	set := UnsafeNew(elements)
	return set.Elems()
}

// CheckUnique is more resource efficient version of Unique. Checks if all elements
// in original slice are unique. Result of that check is returned as a flag
//
// If flag is true all elements in original slice are unique and function
// returns unchanged original slice (with the same underlying pointer).
// If flag is false it means there were duplicates in original slice and
// function returns a new slice (never a sublice) which contains only
// unique elements from original slice
//
// As a special case if original slice contains no elements function returns
// (nil, true). One can describe it as "in an empty set there are no duplicates"
func CheckUnique[ElementType comparable](elements []ElementType) (slice []ElementType, unique bool) {
	set := UnsafeNew(elements)
	if len(set) == 0 {
		return nil, true
	}
	if len(set) == len(elements) {
		return elements, true
	}
	return set.Elems(), false
}

// Retain uses given slice to perform the same operation as CheckUnique.
// To clarify: this function will mutate given slice if it contains
// non-unique elements
//
// Returns number of unique elements and whether or not the given slice
// contained only unique elements
//
// In most cases caller should use this function akin to following example:
//
//	numbers := []int{1, 0, 0, 1, 34, 42}
//	n, ok := sets.Retain(numbers)
//	if !ok {
//		numbers = numbers[:n]
//	}
//	... // numbers slice now only contains 0, 1, 34, 42 (no stable ordering)
func Retain[ElementType comparable](elements []ElementType) (n int, unique bool) {
	set := UnsafeNew(elements)
	if len(set) == 0 {
		return 0, true
	}
	if len(set) == len(elements) {
		return len(elements), true
	}
	elements = elements[:0]
	for element := range set {
		elements = append(elements, element)
	}
	return len(elements), false
}

// FlatUnique combines unique elements from multiple slices into a single slice
func FlatUnique[ElementType comparable](slices [][]ElementType) []ElementType {
	if len(slices) == 0 {
		return nil
	}
	s := Empty[ElementType]()
	for _, slice := range slices {
		s.AddList(slice)
	}
	return s.Elems()
}

// Check checks if all elements are unique in slice. Operates on the same rules
// as function Retain, but does not create or return new slice
func Check[ElementType comparable](elements []ElementType) (unique bool) {
	set := UnsafeNew(elements)
	return len(set) == len(elements)
}

// Add tries to add new element to Set. Does nothing if there already was such element in Set.
// Will panic if used on nil Set
func (s Set[ElementType]) Add(element ElementType) {
	s[element] = struct{}{}
}

// TryAdd tries to add new element to Set. Returns true if this operation actually changed Set.
// Does nothing if there already was such element in Set and returns false.
// Will panic if used on nil Set
func (s Set[ElementType]) TryAdd(element ElementType) bool {
	if s.Has(element) {
		return false
	}
	s[element] = struct{}{}
	return true
}

// Has checks if there is an element in Set. Always returns false on nil Set
func (s Set[ElementType]) Has(element ElementType) bool {
	_, ok := s[element]
	return ok
}

// Rem tries to remove element from Set. Does nothing if there is no such element in Set.
// Does nothing if used on nil Set
func (s Set[ElementType]) Rem(element ElementType) {
	delete(s, element)
}

// TryRem tries to remove element from Set. Returns true if this operation actually changed Set.
// Does nothing if there is no such element in Set and returns false.
// Does nothing if used on nil Set
func (s Set[ElementType]) TryRem(element ElementType) bool {
	if s.Has(element) {
		return false
	}
	delete(s, element)
	return true
}

// AddSet adds all elements from another Set to this one.
// Will panic if used on nil Set
func (s Set[ElementType]) AddSet(set Set[ElementType]) {
	for element := range set {
		s.Add(element)
	}
}

// AddList adds all elements from a slice to this Set.
// Will panic if used on nil Set
func (s Set[ElementType]) AddList(list []ElementType) {
	for _, element := range list {
		s.Add(element)
	}
}

// Push is a convenience function to add multiple elements to Set at once
// Will panic if used on nil Set
func (s Set[ElementType]) Push(elements ...ElementType) {
	for _, element := range elements {
		s.Add(element)
	}
}

// RemSet removes elements which are present in another Set from this one.
// Does nothing if used on nil Set
func (s Set[ElementType]) RemSet(set Set[ElementType]) {
	if len(s) == 0 {
		return
	}
	for element := range set {
		s.Rem(element)
	}
}

// Subset checks if given Set is a subset of this one
//
// Returns true if all elements from given Set are present in this one.
// Always returns true if given Set contains no elements
func (s Set[ElementType]) Subset(set Set[ElementType]) bool {
	if len(set) == 0 {
		return true
	}
	if len(s) < len(set) {
		return false
	}

	for element := range set {
		if !s.Has(element) {
			return false
		}
	}
	return true
}

// Elems turns Set into slice of its elements. Resulting slice is unordered.
// Returns nil if there are no elements in Set or if Set is nil
func (s Set[ElementType]) Elems() []ElementType {
	if len(s) == 0 {
		return nil
	}

	elements := make([]ElementType, 0, len(s))
	for element := range s {
		elements = append(elements, element)
	}

	return elements
}

// Clone returns a new Set with identical elements
func (s Set[ElementType]) Clone() Set[ElementType] {
	if s == nil {
		return nil
	}
	if len(s) == 0 {
		return Empty[ElementType]()
	}

	set := make(Set[ElementType], len(s))
	for element := range s {
		set.Add(element)
	}

	return set
}

// UnsafeClone does the same as Clone, but returns nil if Set contains no elements
func (s Set[ElementType]) UnsafeClone() Set[ElementType] {
	if len(s) == 0 {
		return nil
	}

	set := make(Set[ElementType], len(s))
	for element := range s {
		set.Add(element)
	}

	return set
}

// Equal returns true if both sets has identical elements and false otherwise
func Equal[ElementType comparable](a, b Set[ElementType]) bool {
	if len(a) != len(b) {
		return false
	}
	for element := range a {
		if !b.Has(element) {
			return false
		}
	}
	return true
}

// Union returns a union of two Sets. The resulting Set contains elements which
// are present in at least one of the original Sets
//
// Always returns a new Set. Returns nil if one of Sets is nil and both
// contain no elements
func Union[ElementType comparable](a, b Set[ElementType]) Set[ElementType] {
	if a == nil && len(b) == 0 {
		return nil
	}
	if len(a) == 0 && b == nil {
		return nil
	}
	if len(a) == 0 && len(b) == 0 {
		return Empty[ElementType]()
	}
	if len(a) == 0 {
		// len(b) != 0
		return b.Clone()
	}
	if len(b) == 0 {
		// len(a) != 0
		return a.Clone()
	}

	// after this line (len(a) != 0 && len(b) != 0) is true

	var set Set[ElementType]
	// clone whichever is bigger and add smaller one
	if len(a) < len(b) {
		set = b.Clone()
		set.AddSet(a)
	} else {
		set = a.Clone()
		set.AddSet(b)
	}

	return set
}

// Cross returns an intersection of two Sets. The resulting Set contains elements which
// are present in both of the original Sets
//
// Always returns a new Set. Returns nil if one of Sets is nil
func Cross[ElementType comparable](a, b Set[ElementType]) Set[ElementType] {
	if a == nil || b == nil {
		return nil
	}
	if len(a) == 0 || len(b) == 0 {
		return Empty[ElementType]()
	}

	// after this line (len(a) != 0 && len(b) != 0) is true

	set := Empty[ElementType]()
	// iterate over smaller one
	if len(a) < len(b) {
		for element := range a {
			if b.Has(element) {
				set.Add(element)
			}
		}
	} else {
		for element := range b {
			if a.Has(element) {
				set.Add(element)
			}
		}
	}

	return set
}

// Compare returns a difference between two Sets. The resulting Set contains elements which
// are present in exactly one of the original Sets
//
// If returned Set contains no elements it means Sets are equal. If you need only equality
// check use Equal instead
//
// Always returns a new Set. Returns nil if one of Sets is nil and both
// contain no elements
func Compare[ElementType comparable](a, b Set[ElementType]) Set[ElementType] {
	if a == nil && len(b) == 0 {
		return nil
	}
	if len(a) == 0 && b == nil {
		return nil
	}
	if len(a) == 0 && len(b) == 0 {
		return Empty[ElementType]()
	}

	set := Union(a, b)
	set.RemSet(Cross(a, b))
	return set
}
