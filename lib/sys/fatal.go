package sys

import (
	"fmt"
	"os"
)

func Fatal(v any) {
	fmt.Fprintln(os.Stderr, "fatal:", v)
	os.Exit(1)
}

func Fatalf(format string, v ...any) {
	fmt.Fprintf(os.Stderr, "fatal: "+format+"\n", v...)
	os.Exit(1)
}
