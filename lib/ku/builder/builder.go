package builder

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"codeberg.org/mebyus/goku/lib/ku/ast"
	"codeberg.org/mebyus/goku/lib/ku/builder/urv"
	"codeberg.org/mebyus/goku/lib/ku/genc"
	"codeberg.org/mebyus/goku/lib/ku/ir"
	"codeberg.org/mebyus/goku/lib/ku/ir/cer"
	"codeberg.org/mebyus/goku/lib/ku/ir/origin"
	"codeberg.org/mebyus/goku/lib/ku/parser"
	"codeberg.org/mebyus/goku/lib/ku/source"
	"codeberg.org/mebyus/goku/lib/sets"
	"codeberg.org/mebyus/goku/lib/sys"
)

type Builder struct {
	urv urv.Resolver

	units []*ir.Unit
}

func New() *Builder {
	return &Builder{
		urv: *urv.New(),
	}
}

func (b *Builder) Build(path, outname string) error {
	b.urv.Add(origin.Path{
		Origin:    origin.Loc,
		ImpString: filepath.Clean(path),
	})
	for {
		imp := b.urv.Next()
		if imp.IsEmpty() {
			err := b.traverse()
			if err != nil {
				return err
			}
			err = b.gen(outname)
			if err != nil {
				return err
			}
			return InvokeCC(outname)
		}

		switch imp.Origin {
		case origin.Std:
			panic("not implemented for std")
		case origin.Pkg:
			panic("not implemented for pkg")
		case origin.Loc:
			err := b.build(imp)
			if err != nil {
				return err
			}
		default:
			panic("unreachable: unexpected import origin " + strconv.FormatInt(int64(imp.Origin), 10))
		}
	}
}

// traverse unit dependency graph, check import cycles, break units into
// generation cohorts
func (b *Builder) traverse() error {
	ug, err := b.urv.Traverse()
	if err != nil {
		return err
	}
	toUnit := make(map[origin.Path]*ir.Unit)
	for _, cohort := range ug.Graph.Cohorts {
		for _, i := range cohort {
			node := ug.Graph.Nodes[i]
			if node.Cluster != nil {
				panic("import graph cannot contain clusters")
			}
			imp := ug.Graph.Data[i]
			entry := ug.ByImport[imp]
			if entry.IsEmpty() {
				panic("import graph contains unresolved entry")
			}

			for i := 0; i < len(entry.Imports); i++ {
				p := entry.Imports[i].Path
				u, ok := toUnit[p]
				if !ok {
					panic("unresolved ir unit in import graph")
				}
				entry.Imports[i].Unit = u
			}

			u, err := ir.ConstructUnit(entry.Name, entry.Path.Hash(), entry.Data.([]*ast.UnitSourceCode), entry.Imports)
			if err != nil {
				return err
			}
			toUnit[imp] = u
			b.units = append(b.units, u)
		}
	}
	return nil
}

func (b *Builder) gen(outname string) error {
	file, err := os.Create(outname)
	if err != nil {
		return err
	}
	defer file.Close()

	return genc.Gen(file, b.units)
}

func (b *Builder) build(unit origin.Path) error {
	path := unit.ImpString

	stat, err := os.Stat(path)
	if err != nil {
		return err
	}
	if !stat.IsDir() {
		return fmt.Errorf("file \"%s\" is not a ku unit", path)
	}

	entries, err := os.ReadDir(path)
	if err != nil {
		return err
	}
	if len(entries) == 0 {
		return fmt.Errorf("directory \"%s\" is empty", path)
	}

	var sourceFiles []string
	for _, entry := range entries {
		if !entry.Type().IsRegular() {
			continue
		}
		name := entry.Name()
		if strings.HasSuffix(name, ".ku") {
			sourceFiles = append(sourceFiles, name)
		}
	}
	if len(sourceFiles) == 0 {
		return fmt.Errorf("\"%s\" does not contain ku source files", path)
	}

	var unitName string
	srcs := make([]*ast.UnitSourceCode, 0, len(sourceFiles))
	for _, name := range sourceFiles {
		filename := filepath.Join(path, name)

		file, err := source.Load(filename)
		if err != nil {
			return err
		}
		src, err := parser.ParseSource(file)
		if err != nil {
			return err
		}
		if !src.Unit.Name.Kind.IsEmpty() {
			name := src.Unit.Name.Lit
			if unitName == "" {
				unitName = name
			} else if name != unitName {
				return fmt.Errorf("unit name [%s, %s] is not consistent across files", unitName, name)
			}
		}
		srcs = append(srcs, src)
	}

	if unitName == "" {
		// determine unit name based on directory name
		name := filepath.Base(path)
		if name == "" || name == "." {
			path, err := filepath.Abs(path)
			if err != nil {
				sys.Fatal(err)
			}
			name = filepath.Base(path)
		}
		unitName = name
	}

	importSet := make(sets.Set[origin.Path])
	var imports []ir.Import
	for _, src := range srcs {
		for _, blk := range src.Imports {
			var originName string

			if blk.Origin.Kind.IsEmpty() {
				originName = ""
			} else if blk.Origin.Kind.IsIdent() {
				originName = blk.Origin.Lit
			} else {
				panic(fmt.Sprintf("unexpected origin kind: %s", blk.Origin.Kind.String()))
			}

			orig, ok := origin.Parse(originName)
			if !ok {
				return &cer.Error{
					Kind: cer.UnknownImportOrigin,
					Msg:  blk.Origin.Lit,
					Span: blk.Origin.Pos,
				}
			}

			for _, imp := range blk.Specs {
				str := imp.String.Lit
				if str == "" {
					return &cer.Error{
						Kind: cer.EmptyImportString,
						Span: imp.String.Pos,
					}
				}
				cleaned := filepath.Clean(str)
				if cleaned != str || strings.HasPrefix(str, "/") || strings.HasPrefix(str, ".") {
					return &cer.Error{
						Kind: cer.BadImportString,
						Msg:  str,
						Span: imp.String.Pos,
					}
				}
				if str == path {
					return &cer.Error{
						Kind: cer.SelfImport,
						Msg:  unitName,
						Span: imp.String.Pos,
					}
				}
				elem := origin.Path{
					Origin:    orig,
					ImpString: str,
				}
				if importSet.Has(elem) {
					return &cer.Error{
						Kind: cer.SameStringImport,
						Msg:  str,
						Span: imp.String.Pos,
					}
				}
				importSet.Add(elem)
				imports = append(imports, ir.Import{
					Path: elem,
					Pos:  imp.Name.Pos,
					Name: imp.Name.Lit,
				})
			}
		}
	}
	for _, imp := range imports {
		b.urv.Add(imp.Path)
	}
	b.urv.Bind(urv.Entry{
		Imports: imports, // TODO: add sort by unit path before adding it here
		Path:    unit,
		Name:    unitName,

		Data: srcs,
	})

	return nil
}
