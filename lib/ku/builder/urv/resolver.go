// package urv implements algorithm of resolving import strings to imported unit's code
package urv

import (
	"sort"
	"strings"

	"codeberg.org/mebyus/goku/lib/ku/hoister"
	"codeberg.org/mebyus/goku/lib/ku/ir"
	"codeberg.org/mebyus/goku/lib/ku/ir/cer"
	"codeberg.org/mebyus/goku/lib/ku/ir/origin"
	"codeberg.org/mebyus/goku/lib/sets"
)

type Resolver struct {
	known sets.Set[origin.Path]

	// logical name, technically its a stack with LIFO order
	queue []origin.Path

	// collector field with each unit present exactly once
	deps []Entry

	// maps unit path to unit entry
	byPath map[origin.Path]Entry
}

func SortEntries(entries []Entry) {
	sort.Slice(entries, func(i, j int) bool {
		a := entries[i]
		b := entries[j]
		return a.Origin < b.Origin || (a.Origin == b.Origin && a.ImpString < b.ImpString)
	})
}

// Entry describes dependency relation between unit and
// imports which appear in this unit
type Entry struct {
	Imports []ir.Import

	origin.Path

	Name string

	// parsed unit files
	Data any
}

func toPaths(imps []ir.Import) []origin.Path {
	if len(imps) == 0 {
		return nil
	}
	p := make([]origin.Path, 0, len(imps))
	for _, imp := range imps {
		p = append(p, imp.Path)
	}
	return p
}

func New() *Resolver {
	return &Resolver{
		known:  make(sets.Set[origin.Path]),
		byPath: make(map[origin.Path]Entry),
	}
}

func (r *Resolver) Add(p origin.Path) {
	if r.known.Has(p) {
		return
	}
	r.known.Add(p)
	r.queue = append(r.queue, p)
}

func (r *Resolver) Next() origin.Path {
	if len(r.queue) == 0 {
		return origin.Path{}
	}
	p := r.queue[len(r.queue)-1]
	r.queue = r.queue[:len(r.queue)-1]
	return p
}

// Bind sets dependency imports for specific
func (r *Resolver) Bind(entry Entry) {
	r.deps = append(r.deps, entry)
	r.byPath[entry.Path] = entry
}

type UnitGraph struct {
	Graph *hoister.Graph[origin.Path]

	// maps unit path to unit entry
	ByImport map[origin.Path]Entry
}

func (r *Resolver) Traverse() (UnitGraph, error) {
	SortEntries(r.deps)

	scribe := hoister.New[origin.Path]()
	for _, dep := range r.deps {
		err := scribe.Alpha(dep.Path, toPaths(dep.Imports))
		if err != nil {
			return UnitGraph{}, err
		}
	}

	g, err := scribe.Graph()
	if err != nil {
		return UnitGraph{}, err
	}
	if len(g.Data) != len(g.Nodes) {
		cycle := g.Nodes[len(g.Data)]
		names := make([]string, 0, len(cycle.Cluster))
		for _, idx := range cycle.Cluster {
			names = append(names, r.byPath[g.Data[idx]].Name)
		}
		return UnitGraph{}, &cer.Error{
			Kind: cer.ImportCycle,
			Msg:  strings.Join(names, ", "),
		}
	}
	return UnitGraph{
		Graph:    g,
		ByImport: r.byPath,
	}, nil
}
