package builder

import (
	"os"
	"os/exec"
)

var ccflags = []string{
	"-fwrapv",
	"-Wall",
	"-Wextra",
	"-Wconversion",
	"-Wunreachable-code",
	"-Wshadow",
	"-Wundef",
	"-Wfloat-equal",
	"-Wformat",
	"-Wpointer-arith",
	"-Winit-self",
	"-Wduplicated-branches",
	"-Wduplicated-cond",
	"-Werror",
	"-pipe",
	"-std=c17",
	"-O2",
}

// InvokeCC invoke c compiler on a given file
func InvokeCC(filename string) error {
	args := append(ccflags, "-o", "main.o", "-c", filename)
	cmd := exec.Command("cc", args...)
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	return cmd.Run()
}
