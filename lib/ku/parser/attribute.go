package parser

import (
	"codeberg.org/mebyus/goku/lib/ku/ast"
	"codeberg.org/mebyus/goku/lib/ku/token"
)

type savedAtrBlock struct {
	ast.AtrBlock
	ok bool
}

func (p *Parser) consumeAtrBlock() ast.AtrBlock {
	blk := p.saved.AtrBlock
	p.saved = savedAtrBlock{}
	return blk
}

func (p *Parser) unknownAtr() ([]token.Token, error) {
	var tokens []token.Token
	for {
		if p.tok.Kind == token.Semicolon {
			p.advance() // consume ";"
			return tokens, nil
		}

		tokens = append(tokens, p.tok)
		p.advance()
	}
}

func (p *Parser) atr() (any, error) {
	// TODO: parse at least common atr in form "atr1 = something"
	return p.unknownAtr()
}

func (p *Parser) atrBlock() (ast.AtrBlock, error) {
	p.advance() // consume "atr"
	err := p.expect(token.LeftSquare)
	if err != nil {
		return ast.AtrBlock{}, err
	}

	blk := ast.AtrBlock{Pos: p.tok.Pos}
	p.advance() // consume "["

	for {
		if p.tok.Kind == token.RightSquare {
			p.advance() // consume "]"
			return blk, nil
		}

		atr, err := p.atr()
		if err != nil {
			return ast.AtrBlock{}, err
		}
		blk.List = append(blk.List, atr)
	}
}
