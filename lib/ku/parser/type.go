package parser

import (
	"fmt"

	"codeberg.org/mebyus/goku/lib/ku/ast"
	"codeberg.org/mebyus/goku/lib/ku/token"
)

func (p *Parser) parseTopLevelType(public bool) error {
	p.advance() // skip "type"
	err := p.expect(token.Identifier)
	if err != nil {
		return err
	}
	name := p.ident()
	p.advance() // skip identifier

	spec, err := p.parseTypeSpecifier()
	if err != nil {
		return err
	}

	p.sc.Types = append(p.sc.Types, ast.TypeDefinition{
		Name: name,
		Spec: spec,

		Public: public,
	})
	return nil
}

func (p *Parser) parseTypeSpecifier() (specifier ast.TypeSpecifier, err error) {
	if p.tok.Kind == token.Never {
		specifier = ast.Never(p.tok)
		p.advance() // skip "never"
		return
	}

	if p.tok.Kind == token.Struct {
		return p.structType()
	}

	if p.tok.Kind == token.Identifier {
		return p.parseTypeName()
	}

	if p.tok.Kind == token.Slice {
		specifier, err = p.parseSliceTypeLiteral()
		return
	}

	if p.tok.Kind == token.LeftSquare {
		specifier, err = p.array()
		return
	}

	if p.tok.Kind == token.Asterisk {
		specifier, err = p.parsePointerTypeLiteral()
		return
	}

	if p.tok.Kind == token.LeftParentheses {
		specifier, err = p.tuple()
		return
	}

	err = p.unexpected(p.tok)
	return
}

func (p *Parser) fields(start, stop token.Kind) ([]ast.FieldDefinition, error) {
	err := p.expect(start)
	if err != nil {
		return nil, err
	}
	p.advance() // skip start

	var fields []ast.FieldDefinition
	first := true
	comma := false
	for {
		if p.tok.Kind == stop {
			p.advance() // skip stop
			return fields, nil
		}

		if first {
			first = false
		} else if comma {
			comma = false
		} else {
			return nil, p.unexpected(p.tok)
		}

		var field ast.FieldDefinition
		field, err = p.field()
		if err != nil {
			return nil, err
		}
		fields = append(fields, field)
		if p.tok.Kind == token.Comma {
			comma = true
			p.advance() // skip ","
		}
	}
}

func (p *Parser) structType() (ast.StructType, error) {
	p.advance() // skip "struct"
	fields, err := p.fields(token.LeftCurly, token.RightCurly)
	if err != nil {
		return ast.StructType{}, err
	}
	return ast.StructType{Fields: fields}, nil
}

func (p *Parser) parseTypeName() (name ast.TypeName, err error) {
	ident := p.ident()
	p.advance() // consume identifier
	if p.tok.Kind != token.Period {
		name = ident
		return
	}

	if p.next.Kind != token.Identifier {
		err = fmt.Errorf("unexpected token in qualified type name {%v}", p.next)
		return
	}
	p.advance() // consume "."
	name = ast.QualifiedIdentifier{
		ModuleName: ident,
		Identifier: p.ident(),
	}
	p.advance() // consume identifier
	return
}

func (p *Parser) parseSliceTypeLiteral() (lit ast.SliceTypeLiteral, err error) {
	p.advance() // skip "[]"
	lit.ElementType, err = p.parseTypeSpecifier()
	return
}

func (p *Parser) array() (ast.ArrayTypeLiteral, error) {
	p.advance() // skip "["

	length, err := p.expr()
	if err != nil {
		return ast.ArrayTypeLiteral{}, err
	}

	err = p.expect(token.RightSquare)
	if err != nil {
		return ast.ArrayTypeLiteral{}, err
	}
	p.advance() // skip "]"

	spec, err := p.parseTypeSpecifier()
	if err != nil {
		return ast.ArrayTypeLiteral{}, err
	}

	return ast.ArrayTypeLiteral{
		Length: length,
		Spec:   spec,
	}, nil
}

func (p *Parser) parsePointerTypeLiteral() (lit ast.PointerTypeLiteral, err error) {
	p.advance() // skip "*"
	lit.BaseType, err = p.parseTypeSpecifier()
	return
}
