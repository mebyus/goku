package parser

import (
	"codeberg.org/mebyus/goku/lib/ku/ast"
	"codeberg.org/mebyus/goku/lib/ku/token"
)

func (p *Parser) parseTopLevelFunction(public bool) (err error) {
	p.advance() // consume "fn"
	if p.tok.Kind != token.Identifier {
		return p.unexpected(p.tok)
	}

	declaration := ast.FunctionDeclaration{
		Atrs:   p.consumeAtrBlock(),
		Public: public,
		Name:   p.ident(),
	}
	p.advance() // consume function name identifier

	signature, err := p.parseFunctionSignature()
	if err != nil {
		return
	}
	declaration.Signature = signature

	if p.tok.Kind == token.LeftCurly {
		var body ast.BlockStatement
		body, err = p.block()
		if err != nil {
			return
		}
		definition := ast.FunctionDefinition{
			Declaration: declaration,
			Body:        body,
		}
		p.sc.FunctionDefinitions = append(p.sc.FunctionDefinitions, definition)
		return
	}
	p.sc.FunctionDeclarations = append(p.sc.FunctionDeclarations, declaration)
	return
}

func (p *Parser) parseFunctionSignature() (signature ast.FunctionSignature, err error) {
	parameters, err := p.tuple()
	if err != nil {
		return
	}
	if p.tok.Kind == token.RightArrow {
		p.advance() // skip "=>"
		var result ast.TypeSpecifier
		result, err = p.parseTypeSpecifier()
		if err != nil {
			return
		}
		signature.Result = result
	}
	signature.Parameters = parameters
	return
}

func (p *Parser) unnamedTuple() (tuple ast.UnnamedTuple, err error) {
	for {
		spec, err := p.parseTypeSpecifier()
		if err != nil {
			return nil, err
		}
		tuple = append(tuple, spec)

		if p.tok.Kind == token.Comma && p.next.Kind == token.RightParentheses {
			p.advance() // skip trailing ","
			p.advance() // skip ")"
			return tuple, nil
		}
		if p.tok.Kind == token.RightParentheses {
			p.advance() // skip ")"
			return tuple, nil
		}

		err = p.expect(token.Comma)
		if err != nil {
			return nil, err
		}
		p.advance() // skip ","
	}
}

func (p *Parser) tuple() (tuple ast.Tuple, err error) {
	err = p.expect(token.LeftParentheses)
	if err != nil {
		return
	}
	p.advance() // skip "("

	if p.tok.Kind == token.RightParentheses {
		p.advance() // skip ")"
		return ast.EmptyTuple{}, nil
	}

	// named tuple always starts with a pattern "identifier: ..."
	if !(p.tok.Kind == token.Identifier && p.next.Kind == token.Colon) {
		tuple, err = p.unnamedTuple()
		if err != nil {
			// avoid awkward interface behaviour
			return nil, err
		}
		return tuple, nil
	}

	var fields ast.NamedTuple
	first := true
	comma := false
	for {
		if p.tok.Kind == token.RightParentheses {
			p.advance() // skip ")"
			return fields, nil
		}

		if first {
			first = false
		} else if comma {
			comma = false
		} else {
			err = p.unexpected(p.tok)
			return
		}

		var field ast.FieldDefinition
		field, err = p.field()
		if err != nil {
			return
		}
		fields = append(fields, field)
		if p.tok.Kind == token.Comma {
			comma = true
			p.advance() // skip ","
		}
	}
}

func (p *Parser) field() (field ast.FieldDefinition, err error) {
	err = p.expect(token.Identifier)
	if err != nil {
		return
	}
	field.Name = p.ident()
	p.advance() // skip identifier

	err = p.expect(token.Colon)
	if err != nil {
		return
	}
	p.advance() // consume ":"
	spec, err := p.parseTypeSpecifier()
	if err != nil {
		return
	}
	field.Type = spec
	return
}

// func (p *Parser) parseFunctionParameters() (parameters ast.FunctionParameters, err error) {
// 	p.advance() // consume "("
// 	if p.tok.Kind == token.Identifier {
// 		identifiers := p.parseIdentifierList()
// 		if p.tok.Kind == token.Colon {
// 			parameters, err = p.continueParseNamedFunctionParameters(identifiers)
// 			return
// 		}
// 		if p.tok.Kind == token.RightParentheses {
// 			p.advance() // consume ")"
// 			parameters = convertIdentifiersToTypeSpecifiers(identifiers)
// 			return
// 		}
// 		if p.tok.Kind == token.Period {
// 			parameters, err = p.continueParseTypeSpecifierListFromPeriod(identifiers)
// 			return
// 		}
// 		err = fmt.Errorf("unexpected token {%v} inside function parameters", p.tok)
// 		return
// 	}
// 	parameters, err = p.parseTypeSpecifierList()
// 	if err != nil {
// 		return
// 	}
// 	if p.tok.Kind != token.RightParentheses {
// 		err = fmt.Errorf("expected \")\", found token {%v}", p.tok)
// 		return
// 	}
// 	p.advance() // consume ")"
// 	return
// }

// func (p *Parser) parseIdentifierList() (list []ast.Identifier) {
// 	for {
// 		if p.tok.Kind != token.Identifier {
// 			return
// 		}
// 		list = append(list, p.ident())
// 		p.advance() // consume identifier
// 		if p.tok.Kind == token.Comma {
// 			p.advance() // consume ","
// 		} else {
// 			return
// 		}
// 	}
// }

// func (p *Parser) parseTypeSpecifierListFromInside(prev []ast.TypeSpecifier) (list []ast.TypeSpecifier, err error) {
// 	list = prev
// 	for {
// 		if p.tok.Kind == token.RightParentheses {
// 			// duct tape for detecting end of the list
// 			return
// 		}
// 		var specifier ast.TypeSpecifier
// 		specifier, err = p.parseTypeSpecifier()
// 		if err != nil {
// 			return
// 		}
// 		list = append(list, specifier)
// 		if p.tok.Kind == token.Comma {
// 			p.advance() // consume ","
// 		} else {
// 			return
// 		}
// 	}
// }

// func (p *Parser) parseTypeSpecifierList() (list []ast.TypeSpecifier, err error) {
// 	list, err = p.parseTypeSpecifierListFromInside(nil)
// 	return
// }

// func (p *Parser) continueParseNamedFunctionParameters(identifiers []ast.Identifier) (
// 	parameters ast.NamedFunctionParameters, err error) {

// 	p.advance() // consume ":"
// 	specifier, err := p.parseTypeSpecifier()
// 	if err != nil {
// 		return
// 	}
// 	parameters = append(parameters, ast.ParametersDeclaration{
// 		Names:         identifiers,
// 		TypeSpecifier: specifier,
// 	})
// 	if p.tok.Kind == token.Comma {
// 		p.advance() // consume ","
// 		parameters, err = p.parseNamedFunctionParametersFromInside(parameters)
// 		return
// 	}
// 	if p.tok.Kind != token.RightParentheses {
// 		err = fmt.Errorf("unexpected token {%v} at the end of named function parameters", p.tok)
// 		return
// 	}
// 	p.advance() // consume ")"
// 	return
// }

// func (p *Parser) continueParseTypeSpecifierListFromPeriod(
// 	identifiers []ast.Identifier) (specifiers []ast.TypeSpecifier, err error) {

// 	p.advance() // consume "."
// 	if p.tok.Kind != token.Identifier {
// 		err = fmt.Errorf("expected identifier after \".\" in qualified identifier, got {%v}", p.tok)
// 		return
// 	}
// 	ident := p.ident()
// 	p.advance() // consume identifier
// 	lastIndex := len(identifiers) - 1
// 	lastIdentifier := identifiers[lastIndex]
// 	qualifiedIdentifier := ast.QualifiedIdentifier{
// 		ModuleName: lastIdentifier,
// 		Identifier: ident,
// 	}
// 	specifiers = convertIdentifiersToTypeSpecifiers(identifiers[:lastIndex])
// 	specifiers = append(specifiers, qualifiedIdentifier)
// 	if p.tok.Kind == token.Comma {
// 		p.advance() // consume ","
// 		specifiers, err = p.parseTypeSpecifierListFromInside(specifiers)
// 		return
// 	}
// 	if p.tok.Kind != token.RightParentheses {
// 		err = fmt.Errorf("unexpected token {%v} at the end of named function parameters", p.tok)
// 		return
// 	}
// 	p.advance() // consume ")"
// 	return
// }

// func convertIdentifiersToTypeSpecifiers(identifiers []ast.Identifier) (specifiers []ast.TypeSpecifier) {
// 	specifiers = make([]ast.TypeSpecifier, len(identifiers))
// 	for i := 0; i < len(identifiers); i++ {
// 		specifiers[i] = identifiers[i]
// 	}
// 	return
// }
