package parser

import (
	"io"

	"codeberg.org/mebyus/goku/lib/ku/ast"
	"codeberg.org/mebyus/goku/lib/ku/ir/cer"
	"codeberg.org/mebyus/goku/lib/ku/lexer"
	"codeberg.org/mebyus/goku/lib/ku/source"
	"codeberg.org/mebyus/goku/lib/ku/token"
)

type Parser struct {
	lx lexer.Stream
	sc *ast.UnitSourceCode

	saved savedAtrBlock

	// previous token
	prev token.Token

	// token at current Parser position
	tok token.Token

	// next token
	next token.Token
}

func New(lx lexer.Stream) *Parser {
	p := &Parser{
		lx: lx,
		sc: &ast.UnitSourceCode{},
	}

	// init parser buffer
	p.advance()
	p.advance()
	return p
}

func FromReader(r io.Reader) (*Parser, error) {
	lx, err := lexer.FromReader(r)
	if err != nil {
		return nil, err
	}
	return New(lx), nil
}

func FromBytes(b []byte) *Parser {
	return New(lexer.FromBytes(b))
}

func FromFile(filename string) (p *Parser, err error) {
	lx, err := lexer.FromFile(filename)
	if err != nil {
		return
	}
	p = New(lx)
	return p, nil
}

func FromSource(src *source.File) (p *Parser, err error) {
	lx := lexer.FromSource(src)
	p = New(lx)
	p.sc.File = src
	return p, nil
}

func ParseBytes(b []byte) (sc *ast.UnitSourceCode, err error) {
	p := FromBytes(b)
	return p.parse()
}

func ParseFile(filename string) (sc *ast.UnitSourceCode, err error) {
	p, err := FromFile(filename)
	if err != nil {
		return
	}
	return p.parse()
}

func ParseSource(src *source.File) (sc *ast.UnitSourceCode, err error) {
	p, err := FromSource(src)
	if err != nil {
		return
	}
	return p.parse()
}

func Parse(r io.Reader) (sc *ast.UnitSourceCode, err error) {
	p, err := FromReader(r)
	if err != nil {
		return
	}
	sc, err = p.parse()
	return
}

func (p *Parser) parse() (sc *ast.UnitSourceCode, err error) {
	ok, err := p.parseUnitClause()
	if err != nil {
		return
	}
	if ok {
		err = p.parseInUnitMode()
	} else {
		err = p.parseInNoUnitMode()
	}
	if err != nil {
		return
	}
	if p.saved.ok {
		return nil, &cer.Error{
			Kind: cer.OrphanedAtrBlock,
			Span: p.saved.Pos,
		}
	}
	sc = p.sc
	return
}
