package parser

import (
	"codeberg.org/mebyus/goku/lib/ku/ir/cer"
	"codeberg.org/mebyus/goku/lib/ku/token"
)

func (p *Parser) unexpected(tok token.Token) *cer.Error {
	return &cer.Error{
		Span: tok,
		Msg:  tok.Kind.String(),
		Kind: cer.UnexpectedToken,
		Info: tok,
	}
}
