package parser

import (
	"codeberg.org/mebyus/goku/lib/ku/ast"
	"codeberg.org/mebyus/goku/lib/ku/token"
)

func (p *Parser) tupleLiteral() (ast.TupleLiteral, error) {
	p.advance() // skip "("

	var exprs []ast.Expression
	first := true
	comma := false
	for {
		if p.tok.Kind == token.RightParentheses {
			p.advance() // consume ")"
			return ast.TupleLiteral{List: exprs}, nil
		}

		if first {
			first = false
		} else if comma {
			comma = false
		} else {
			return ast.TupleLiteral{}, p.unexpected(p.tok)
		}

		expr, err := p.expr()
		if err != nil {
			return ast.TupleLiteral{}, err
		}
		exprs = append(exprs, expr)
		if p.tok.Kind == token.Comma {
			comma = true
			p.advance() // skip ","
		}
	}
}
