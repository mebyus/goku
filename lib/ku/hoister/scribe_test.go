package hoister

import (
	"reflect"
	"testing"
)

func TestGraph(t *testing.T) {
	type vertex struct {
		id  int
		anc []int // list of ancestor ids
		k   Kind
	}
	tests := []struct {
		name string
		vx   []vertex
		want *Graph[int]
	}{
		{
			name: "1 single alpha vertex",
			vx: []vertex{
				{
					id: 1,
					k:  alpha,
				},
			},
			want: &Graph[int]{
				Data: []int{1},
				Cohorts: [][]int{
					{0},
				},
				Backbone: Backbone{
					Nodes: []UnifiedNode{
						{
							Index:  0,
							Kind:   alpha,
							Cohort: 0,
						},
					},
					Roots: []int{0},
				},
			},
		},
		{
			name: "2 single beta vertex",
			vx: []vertex{
				{
					id: 1,
					k:  beta,
				},
			},
			want: &Graph[int]{
				Data: []int{1},
				Cohorts: [][]int{
					{0},
				},
				Backbone: Backbone{
					Nodes: []UnifiedNode{
						{
							Index:  0,
							Kind:   beta,
							Cohort: 0,
						},
					},
					Roots: []int{0},
				},
			},
		},
		{
			name: "3 two alpha root vertices",
			vx: []vertex{
				{
					id: 1,
					k:  alpha,
				},
				{
					id: 2,
					k:  alpha,
				},
			},
			want: &Graph[int]{
				Data: []int{1, 2},
				Cohorts: [][]int{
					{0, 1},
				},
				Backbone: Backbone{
					Nodes: []UnifiedNode{
						{
							Index:  0,
							Kind:   alpha,
							Cohort: 0,
						},
						{
							Index:  1,
							Kind:   alpha,
							Cohort: 0,
						},
					},
					Roots: []int{0, 1},
				},
			},
		},
		{
			name: "4 two vertices",
			vx: []vertex{
				{
					id: 1,
					k:  alpha,
				},
				{
					id:  2,
					k:   alpha,
					anc: []int{1},
				},
			},
			want: &Graph[int]{
				Data: []int{1, 2},
				Cohorts: [][]int{
					{0},
					{1},
				},
				Backbone: Backbone{
					Nodes: []UnifiedNode{
						{
							Index:       0,
							Kind:        alpha,
							Cohort:      0,
							Descendants: []int{1},
						},
						{
							Index:     1,
							Kind:      alpha,
							Cohort:    1,
							Ancestors: []int{0},
						},
					},
					Roots: []int{0},
				},
			},
		},
		{
			name: "5 five vertices",
			vx: []vertex{
				{
					id: 1,
					k:  beta,
				},
				{
					id: 2,
					k:  beta,
				},
				{
					id:  3,
					k:   alpha,
					anc: []int{1},
				},
				{
					id:  4,
					k:   beta,
					anc: []int{1, 2},
				},
				{
					id:  5,
					k:   beta,
					anc: []int{3, 4, 2},
				},
			},
			want: &Graph[int]{
				Data: []int{1, 2, 3, 4, 5},
				Cohorts: [][]int{
					{0, 1},
					{2, 3},
					{4},
				},
				Backbone: Backbone{
					Nodes: []UnifiedNode{
						{
							Index:       0,
							Kind:        beta,
							Cohort:      0,
							Descendants: []int{2, 3},
						},
						{
							Index:       1,
							Kind:        beta,
							Cohort:      0,
							Descendants: []int{3, 4},
						},
						{
							Index:       2,
							Kind:        alpha,
							Cohort:      1,
							Ancestors:   []int{0},
							Descendants: []int{4},
						},
						{
							Index:       3,
							Kind:        beta,
							Cohort:      1,
							Ancestors:   []int{0, 1},
							Descendants: []int{4},
						},
						{
							Index:     4,
							Kind:      beta,
							Cohort:    2,
							Ancestors: []int{2, 3, 1},
						},
					},
					Roots: []int{0, 1},
				},
			},
		},
		{
			name: "6 single node cycle",
			vx: []vertex{
				{
					id:  1,
					k:   beta,
					anc: []int{1},
				},
			},
			want: &Graph[int]{
				Data: []int{1},
				Cohorts: [][]int{
					{1},
				},
				Backbone: Backbone{
					Nodes: []UnifiedNode{
						{
							Index:       0,
							Kind:        beta,
							Cohort:      0,
							Ancestors:   []int{0},
							Descendants: []int{0},
						},
						{
							Index:   1,
							Kind:    cluster,
							Cohort:  0,
							Cluster: []int{0},
						},
					},
					Roots: []int{1},
				},
			},
		},
		{
			name: "7 two nodes cycle",
			vx: []vertex{
				{
					id:  1,
					k:   beta,
					anc: []int{2},
				},
				{
					id:  2,
					k:   beta,
					anc: []int{1},
				},
			},
			want: &Graph[int]{
				Data: []int{1, 2},
				Cohorts: [][]int{
					{2},
				},
				Backbone: Backbone{
					Nodes: []UnifiedNode{
						{
							Index:       0,
							Kind:        beta,
							Cohort:      0,
							Ancestors:   []int{1},
							Descendants: []int{1},
						},
						{
							Index:       1,
							Kind:        beta,
							Cohort:      0,
							Ancestors:   []int{0},
							Descendants: []int{0},
						},
						{
							Index:   2,
							Kind:    cluster,
							Cohort:  0,
							Cluster: []int{0, 1},
						},
					},
					Roots: []int{2},
				},
			},
		},
		{
			name: "8 two nodes cycle",
			vx: []vertex{
				{
					id: 1,
					k:  beta,
				},
				{
					id:  2,
					k:   beta,
					anc: []int{1, 3},
				},
				{
					id:  3,
					k:   beta,
					anc: []int{2},
				},
			},
			want: &Graph[int]{
				Data: []int{1, 2, 3},
				Cohorts: [][]int{
					{0},
					{3},
				},
				Backbone: Backbone{
					Nodes: []UnifiedNode{
						{
							Index:       0,
							Kind:        beta,
							Cohort:      0,
							Descendants: []int{1},
						},
						{
							Index:       1,
							Kind:        beta,
							Cohort:      1,
							Ancestors:   []int{0, 2},
							Descendants: []int{2},
						},
						{
							Index:       2,
							Kind:        beta,
							Cohort:      1,
							Ancestors:   []int{1},
							Descendants: []int{1},
						},
						{
							Index:     3,
							Kind:      cluster,
							Cohort:    1,
							Ancestors: []int{0},
							Cluster:   []int{1, 2},
						},
					},
					Roots: []int{0},
				},
			},
		},
		{
			name: "9 two nodes root cycle",
			vx: []vertex{
				{
					id:  1,
					k:   beta,
					anc: []int{2},
				},
				{
					id:  2,
					k:   beta,
					anc: []int{1},
				},
				{
					id:  3,
					k:   alpha,
					anc: []int{2},
				},
			},
			want: &Graph[int]{
				Data: []int{1, 2, 3},
				Cohorts: [][]int{
					{3},
					{2},
				},
				Backbone: Backbone{
					Nodes: []UnifiedNode{
						{
							Index:       0,
							Kind:        beta,
							Cohort:      0,
							Ancestors:   []int{1},
							Descendants: []int{1},
						},
						{
							Index:       1,
							Kind:        beta,
							Cohort:      0,
							Ancestors:   []int{0},
							Descendants: []int{0, 2},
						},
						{
							Index:     2,
							Kind:      alpha,
							Cohort:    1,
							Ancestors: []int{1},
						},
						{
							Index:       3,
							Kind:        cluster,
							Cohort:      0,
							Cluster:     []int{0, 1},
							Descendants: []int{2},
						},
					},
					Roots: []int{3},
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(
			tt.name,
			func(t *testing.T) {
				s := New[int]()
				for _, v := range tt.vx {
					err := s.add(v.k, v.id, v.anc)
					if err != nil {
						t.Errorf("add vertex should not fail, err = %v", err)
						return
					}
				}
				got, err := s.Graph()
				if err != nil {
					t.Errorf("graph construction should not fail, err = %v", err)
					return
				}
				if !reflect.DeepEqual(got, tt.want) {
					t.Errorf("\nGraph() = %v\nwant      %v", got, tt.want)
					return
				}
			},
		)
	}
}
