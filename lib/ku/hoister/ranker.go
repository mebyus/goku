package hoister

// Ranker traverses Nodes to determine their Cohort values
type Ranker struct {
	// Indicates how many ancestors are still unranked for a Node with
	// particular index, directly corresponding to index in this slice.
	// If Node was already ranked corresponding left value will be 0
	left []int

	// maps Node index to a list of other Node indices
	//
	//	k => v
	//
	// if k is present in this map it means that list of Nodes with
	// indices present in v are waiting for Node with index k to be
	// ranked to in turn advance their ranking further
	queue map[int][]int

	// maps regular Node index to index of cluster Node if the former
	// belongs to cluster, otherwise index is not present in this map
	//
	// key in this map cannot be an index of cluster Node
	clusters map[int]int

	// list of Node indices which will be the source of next wave
	wave []int

	// buffer for preparing next wave
	next []int

	cohorts [][]int

	*Backbone
}

func NewRanker(bb *Backbone) *Ranker {
	return &Ranker{
		left:     make([]int, len(bb.Nodes)),
		queue:    make(map[int][]int, len(bb.Nodes)),
		clusters: make(map[int]int),

		Backbone: bb,
	}
}

// sets rank for specific Node index
func (r *Ranker) set(i, rank int) {
	for j := len(r.cohorts); j <= rank; j++ {
		r.cohorts = append(r.cohorts, nil)
	}
	r.cohorts[rank] = append(r.cohorts[rank], i)
	if r.Nodes[i].Kind == cluster {
		for _, n := range r.Nodes[i].Cluster {
			r.Nodes[n].Cohort = rank
		}
	}
}

func (r *Ranker) swap() {
	r.wave = r.next
	r.next = nil
}

func (r *Ranker) Rank() [][]int {
	for i, node := range r.Nodes {
		r.left[i] = len(node.Ancestors)
		if len(node.Descendants) != 0 {
			r.queue[i] = node.Descendants
		}
		if node.Kind == cluster {
			for _, n := range node.Cluster {
				r.clusters[n] = i
			}
		}
	}

	r.cohorts = make([][]int, 1)
	r.cohorts[0] = append(r.cohorts[0], r.Roots...)

	r.wave = r.Roots
	for len(r.wave) != 0 {
		for _, i := range r.wave {
			waiters, ok := r.queue[i]
			if !ok {
				continue
			}

			rank := r.Nodes[i].Cohort + 1

			delete(r.queue, i)
			for _, j := range waiters {
				c, ok := r.clusters[j]
				if ok {
					j = c
				}
				r.left[j] -= 1
				if rank > r.Nodes[j].Cohort {
					r.Nodes[j].Cohort = rank
				}
				if r.left[j] == 0 {
					r.set(j, r.Nodes[j].Cohort)
					r.next = append(r.next, j)
				}
			}
		}

		r.swap()
	}

	return r.cohorts
}
