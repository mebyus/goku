package hoister

// Kind indicates Node kind
type Kind uint8

const (
	// Nodes and UnifiedNodes of this Kind cannot form a cluster
	alpha Kind = iota

	// Nodes and UnifiedNodes of this Kind can form a cluster
	beta

	// Clustered UnifiedNode consists of other UnifiedNodes
	cluster
)

// UnifiedNode stores regular Nodes and Clusters in the same datastructure and
// thus allows Graph analysis with the same code for each Node Kind, without
// storing Node data within that Node
type UnifiedNode struct {
	// Contains indices of this UnifiedNode's ancestors
	//
	// For cluster Kind it is a collection of all ancestors outside of cluster
	// for each Node forming a cluster
	Ancestors []int

	// Contains indices of this UnifiedNode's descendants
	//
	// For cluster Kind it is a collection of all descendants outside of cluster
	// for each Node forming a cluster
	Descendants []int

	// Always nil if Kind is alpha or beta
	//
	// Contains indices of regular UnifiedNodes which form this cluster.
	// Contains one or more elements if Kind is cluster. Cannot contain
	// UnifiedNodes of cluster Kind
	Cluster []int

	// Slice element index where Node data is stored. What slice should be
	// looked for data is determined by Kind
	//
	// For cluster Kind this value field is always greater than that of any
	// regular Node. For cluster Kind data slice does not have corresponding
	// element and will panic if accessed
	Index int

	// For current UnifiedNode equals max Cohort among its Ancestors plus one
	Cohort int

	Kind Kind
}

// Node represents a regular (not clustered) Graph Node
type Node struct {
	Ancestors   []*Node
	Descendants []*Node

	// Stores index of this Node within list of Graph Nodes
	Index int

	// Can only be alpha or beta
	Kind Kind
}

func listNodesIndices(nodes []*Node) []int {
	if len(nodes) == 0 {
		return nil
	}

	indices := make([]int, 0, len(nodes))
	for _, n := range nodes {
		indices = append(indices, n.Index)
	}
	return indices
}

// get list of Node's ancestor indices
func (n *Node) ListAncestors() []int {
	return listNodesIndices(n.Ancestors)
}

// get list of Node's ancestor indices
func (n *Node) ListDescendants() []int {
	return listNodesIndices(n.Descendants)
}
