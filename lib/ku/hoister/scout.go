package hoister

import "sort"

type Backbone struct {
	Nodes []UnifiedNode

	// indices of root Nodes in Graph
	Roots []int
}

type Graph[T comparable] struct {
	Backbone

	Data []T

	// Each element is a slice of indices with that element index cohort value.
	// For example Cohorts[1] gives indices of all Nodes with Cohort = 1
	Cohorts [][]int
}

// Scout traverses Graph to find cycles in it. Found cycles are merged
// into Graoh as UnifiedNodes
type Scout struct {
	// stores Scout's path in Graph
	stack []ScoutPos

	// maps Node index to its index in stack (only if Node is present in stack)
	index map[int]int

	// maps regular Node index to index of cluster Node if the former
	// belongs to cluster, otherwise index is not present in this map
	//
	// key in this map cannot be an index of cluster Node
	clusters map[int]int

	// indicates whether a Node was already visited or not
	//
	// element at specific index always corresponds to Node with
	// the same index
	visited []bool

	*Backbone
}

func NewScout(bb *Backbone) *Scout {
	return &Scout{
		index:    make(map[int]int),
		clusters: make(map[int]int),
		visited:  make([]bool, len(bb.Nodes)),

		Backbone: bb,
	}
}

type ScoutPos struct {
	// index of Node
	i int

	// index of next Node descendant (to clarify: this value is not a Node index)
	next int
}

func (s *Scout) isEmpty() bool {
	return len(s.stack) == 0
}

// push Node's index onto the stack
func (s *Scout) push(i int) {
	s.visited[i] = true
	s.index[i] = len(s.stack)
	s.stack = append(s.stack, ScoutPos{i: i})
}

func (s *Scout) pop() {
	tip := s.tip()
	delete(s.index, tip.i)
	s.stack = s.stack[:len(s.stack)-1]
}

func (s *Scout) tip() ScoutPos {
	return s.stack[len(s.stack)-1]
}

// gives Node's descendants list of indices
func (s *Scout) desc(i int) []int {
	c, ok := s.clusters[i]
	if ok {
		return s.Nodes[c].Descendants
	}
	return s.Nodes[i].Descendants
}

// gives Node's ancestors list of indices
func (s *Scout) anc(i int) []int {
	return s.Nodes[i].Ancestors
}

func (s *Scout) next() int {
	tip := s.tip()
	next := tip.next
	tip.next++
	s.stack[len(s.stack)-1] = tip
	return next
}

func (s *Scout) step() ScoutStep {
	tip := s.tip()
	desc := s.desc(tip.i)

	if len(desc) == 0 {
		return ScoutStep{Kind: ascend}
	}

	j := s.next()
	for j < len(desc) {
		// next Node's index in path chosen among current Node's descendants
		next := desc[j]
		c, ok := s.clusters[next]
		if ok {
			next = c
		}
		if !s.visited[next] {
			return ScoutStep{
				Val:  next,
				Kind: descend,
			}
		}

		k, ok := s.index[next]
		if ok {
			// cycle found
			return ScoutStep{
				Val:  k,
				Kind: cycle,
			}
		}

		j = s.next()
	}

	return ScoutStep{Kind: ascend}
}

type StepKind uint8

const (
	// scout descends, its path length increases
	descend StepKind = iota

	// scout ascends, its path length decreases
	ascend

	// scout found cycle in its path
	cycle
)

type ScoutStep struct {
	// meaning depends on Kind
	//
	//	descend: next Node index
	//	ascend: ignored
	//	cycle: stack index of cycle start in scout's path
	Val int

	Kind StepKind
}

func (s *Scout) Traverse() error {
	for _, root := range s.Roots {
		s.traverse(root)
	}

	for i := 0; i < len(s.visited); i++ {
		if !s.visited[i] {
			s.traverse(i)
		}
	}

	return nil
}

// traverse Graph starting from Node with specified index
func (s *Scout) traverse(i int) {
	s.push(i)

	for !s.isEmpty() {
		step := s.step()

		switch step.Kind {
		case descend:
			s.push(step.Val)
		case ascend:
			s.pop()
		case cycle:
			s.handle(step.Val)
		default:
			panic(step.Kind)
		}
	}
}

// handle found cycle in scout's path
//
// argument is a stack index of cycle start, it is assumed that
// Nodes up until after top of the stack form cycle
func (s *Scout) handle(p int) {
	// list of Node indices which form cycle
	nodes := make([]int, 0, len(s.stack)-p)

	// indices of cluster Nodes if Node of said type is already present
	// in detected cycle
	var clusters []int

	// list of next descendant indices for Nodes which form cycle
	var nlist []int

	for i := p; i < len(s.stack); i++ {
		j := s.stack[i].i
		next := s.stack[i].next
		if s.Nodes[j].Kind == cluster {
			clusters = append(clusters, j)
		}
		nodes = append(nodes, j)
		nlist = append(nlist, next)
	}

	if len(clusters) == 0 {
		// there are no cluster Nodes in cycle, therefore this cycle
		// creates new cluster

		// index of new cluster Node
		c := len(s.Nodes)

		for _, n := range nodes {
			s.clusters[n] = c
			delete(s.index, n)
		}

		// set of checked merged descendants
		cset := make(map[int]struct{})

		// set of unchecked merged descendants
		uset := make(map[int]struct{})

		// set of merged ancestors
		aset := make(map[int]struct{})

		for i, n := range nodes {
			for _, a := range s.anc(n) {
				k, ok := s.clusters[a]
				if ok && k == c {
					// do not add nodes which are in the same cluster
					continue
				}

				aset[a] = struct{}{}
			}

			for _, d := range s.Nodes[n].Descendants[:nlist[i]] {
				k, ok := s.clusters[d]
				if ok && k == c {
					// do not add nodes which are in the same cluster
					continue
				}

				cset[d] = struct{}{}
			}

			for _, d := range s.Nodes[n].Descendants[nlist[i]:] {
				k, ok := s.clusters[d]
				if ok && k == c {
					// do not add nodes which are in the same cluster
					continue
				}

				uset[d] = struct{}{}
			}
		}

		// merged list of regular Node ancestor of cluster Nodes indices
		var ancestors []int
		if len(aset) != 0 {
			ancestors = make([]int, 0, len(aset))
			for a := range aset {
				ancestors = append(ancestors, a)
			}
			sort.Ints(ancestors)
		}

		if len(ancestors) == 0 {
			s.Roots = append(s.Roots, c)
		}

		// merged list of regular Node descendant of cluster Nodes indices
		var descendants []int
		if len(cset)+len(uset) != 0 {
			descendants = make([]int, 0, len(cset)+len(uset))
			for d := range cset {
				descendants = append(descendants, d)
			}
			for d := range uset {
				descendants = append(descendants, d)
			}
		}

		s.Nodes = append(s.Nodes, UnifiedNode{
			Ancestors:   ancestors,
			Descendants: descendants,

			Cluster: nodes,
			Index:   c,
			Kind:    cluster,
		})
		s.stack = s.stack[:p]
		s.index[c] = len(s.stack)
		s.stack = append(s.stack, ScoutPos{
			i:    c,
			next: len(cset),
		})
		s.visited = append(s.visited, true)
		return
	}

	// we need to merge cycle into existing cluster
	panic("not implemented")
}
