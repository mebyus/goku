package hoister

import "fmt"

// Scribe constructs a proper graph from given Nodes. Most importantly
// it identifies clusters in directed graph and calculates cohort value
// of each Node
type Scribe[T comparable] struct {
	// maps Node data to its index in Nodes slice
	Index map[T]int

	// Order and number of elements mirrors Nodes slice such that
	// Data[i] gives Nodes[i] stored data
	Data []T

	// List of all Nodes in Graph
	Nodes []*Node

	// List of Nodes with no ancestors
	Roots []*Node

	// Set of all Node data that were added as self
	uniqueCheck map[T]struct{}
}

func New[T comparable]() *Scribe[T] {
	return &Scribe[T]{
		Index: make(map[T]int),

		uniqueCheck: make(map[T]struct{}),
	}
}

func (s *Scribe[T]) Alpha(self T, ancestors []T) error {
	return s.add(alpha, self, ancestors)
}

func (s *Scribe[T]) Beta(self T, ancestors []T) error {
	return s.add(beta, self, ancestors)
}

func (s *Scribe[T]) add(k Kind, self T, ancestors []T) error {
	_, ok := s.uniqueCheck[self]
	if ok {
		return fmt.Errorf("node cannot be added second time as self")
	}
	s.uniqueCheck[self] = struct{}{}

	var node *Node
	index, ok := s.Index[self]
	if !ok {
		index = len(s.Nodes)
		node = &Node{
			Index: index,
			Kind:  k,
		}
		s.Index[self] = index
		s.Data = append(s.Data, self)
		s.Nodes = append(s.Nodes, node)
	} else {
		node = s.Nodes[index]
	}

	if len(ancestors) == 0 {
		s.Roots = append(s.Roots, s.Nodes[index])
	}

	for _, a := range ancestors {
		if k == alpha && a == self {
			return fmt.Errorf("alpha node cannot be ancestor of itself")
		}

		var n *Node
		i, ok := s.Index[a]
		if !ok {
			i = len(s.Nodes)
			n = &Node{
				Index: i,
				Kind:  k,
			}
			s.Index[a] = i
			s.Data = append(s.Data, a)
			s.Nodes = append(s.Nodes, n)
		} else {
			n = s.Nodes[i]
		}

		node.Ancestors = append(node.Ancestors, n)
		n.Descendants = append(n.Descendants, node)
	}

	return nil
}

func (s *Scribe[T]) Graph() (*Graph[T], error) {
	g, err := s.scribe()
	if err != nil {
		return nil, err
	}
	scout := NewScout(&g.Backbone)
	err = scout.Traverse()
	if err != nil {
		return nil, err
	}
	ranker := NewRanker(&g.Backbone)
	g.Cohorts = ranker.Rank()
	return g, nil
}

func (s *Scribe[T]) scribe() (*Graph[T], error) {
	if len(s.Nodes) == 0 {
		return nil, fmt.Errorf("graph is empty")
	}
	for _, d := range s.Data {
		_, ok := s.uniqueCheck[d]
		if !ok {
			return nil, fmt.Errorf("graph has ghost node (referenced, but not added)")
		}
	}

	nodes := make([]UnifiedNode, 0, len(s.Nodes))
	for _, n := range s.Nodes {
		nodes = append(nodes, UnifiedNode{
			Ancestors:   n.ListAncestors(),
			Descendants: n.ListDescendants(),
			Index:       n.Index,
			Kind:        n.Kind,
		})
	}

	return &Graph[T]{
		Backbone: Backbone{
			Roots: listNodesIndices(s.Roots),
			Nodes: nodes,
		},
		Data: s.Data,
	}, nil
}
