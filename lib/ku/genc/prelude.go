package genc

const prelude = `
// Use only for comparison with pointer types
#define nil 0

// Macro for type cast
#define cast(T, x) (T)(x)

// Macro to compute chunk size in bytes to hold n elements of type T
#define chunk_size(T, n) ((n) * sizeof(T))

typedef unsigned char u8;
typedef unsigned short int u16;
typedef unsigned int u32;
typedef unsigned long long int u64;

typedef signed char i8;
typedef signed short int i16;
typedef signed int i32;
typedef signed long long int i64;

typedef float f32;
typedef double f64;

// Platform dependent unsigned integer type. Always have enough bytes to
// represent size of any memory region (or offset in memory region)
typedef u64 usz;
typedef i64 isz;

typedef u8 bool;

struct str {
	u8* bytes;
	usz len;
};

typedef struct str str;

const bool false = 0;
const bool true = 1;
`
