package genc

import (
	"fmt"
	"io"
	"strconv"

	"codeberg.org/mebyus/goku/lib/ku/ir"
	"codeberg.org/mebyus/goku/lib/ku/ir/scope"
	"codeberg.org/mebyus/goku/lib/ku/ir/symbol"
	"codeberg.org/mebyus/goku/lib/ku/ir/typ"
)

func Gen(w io.Writer, units []*ir.Unit) error {
	buf := NewBuffer(len(prelude))
	buf.write(prelude)
	buf.nl()

	for _, unit := range units {
		buf.comment(fmt.Sprintf("unit %s", unit.Name))
		for _, cohort := range unit.Graph.Cohorts {
			buf.nl()
			buf.comment(fmt.Sprintf("cohort %v", cohort))
			for _, i := range cohort {
				node := unit.Graph.Nodes[i]
				if node.Cluster == nil {
					sym := unit.Graph.Data[i]
					buf.top(sym)
					buf.nl()
					buf.nl()
				} else {
					buf.comment(fmt.Sprintf("cluster %v", cohort))
					for _, j := range node.Cluster {
						sym := unit.Graph.Data[j]
						buf.top(sym)
						buf.nl()
						buf.nl()
					}
					buf.nl()
				}
			}
		}
	}

	_, err := w.Write(buf.buf)
	return err
}

func (b *Buffer) top(sym *ir.Symbol) {
	switch sym.Kind {
	case symbol.Type:
		b.typ(sym)
	case symbol.Fn:
		b.fn(sym)
	case symbol.Const:
		b.comment("const " + b.lookupName(sym))
	case symbol.Var:
		b.comment("var " + b.lookupName(sym))
	default:
		panic(fmt.Sprintf("not implemented for %d", sym.Kind))
	}
}

func (b *Buffer) lookupName(sym *ir.Symbol) string {
	name, ok := b.names[sym]
	if ok {
		return name
	}

	if sym.Scope.Kind == scope.Unit {
		name = "ku_" + sym.Unit.Name + "_" + sym.Name
	} else {
		name = sym.Name
	}
	b.names[sym] = name
	return name
}

func (b *Buffer) lookupTypeSpec(t *typ.Type) string {
	s, ok := b.types[t]
	if ok {
		return s
	}

	switch t.Kind {
	case typ.Zero:
		s = "void"
	case typ.Signed, typ.Unsigned:
		s = t.Name
	case typ.Boolean:
		s = "bool"
	case typ.Chunk:
		s = "chunk_" + b.lookupTypeSpec(t.Def.(*typ.Type))
	case typ.Struct:
		fields := t.Def.(typ.StructSpec).Fields
		b.write("{")
		b.nl()
		b.inc()
		for _, field := range fields {
			b.idn()
			b.write(b.lookupTypeSpec(field.Type))
			b.space()
			b.write(field.Name)
			b.write(";")
			b.nl()
		}
		b.dec()
		b.idn()
		b.write("}")
	case typ.Named:
		s = t.Name
	case typ.StaticInteger:
		s = "isz"
	default:
		panic(fmt.Sprintf("not implemented for %s", t.Kind.String()))
	}

	b.types[t] = s
	return s
}

func (b *Buffer) spec(t *typ.Type) {
	b.write(b.lookupTypeSpec(t))
}

func (b *Buffer) typ(sym *ir.Symbol) {
	t := sym.Def.(*typ.Type)
	if t.Kind != typ.Named {
		panic(fmt.Sprintf("expected named type, got %s", t.Kind.String()))
	}
	b.write("typedef ")
	if t.Base.Kind == typ.Struct {
		b.write("struct ")
		b.write(t.Name)
		b.space()
	}
	b.spec(t.Base)
	b.space()
	b.write(b.lookupName(sym))
	b.write(";")
}

func (b *Buffer) fn(sym *ir.Symbol) {
	fn := sym.Def.(*ir.Fn)

	b.spec(fn.Type)
	b.space()
	b.write(b.lookupName(sym))
	b.args(fn.Args)
	b.space()
	b.block(&fn.Body)
}

func (b *Buffer) args(args []*ir.Arg) {
	b.write("(")
	if len(args) != 0 {
		b.arg(args[0])
		for i := 1; i < len(args); i++ {
			b.write(", ")
			b.arg(args[i])
		}
	}
	b.write(")")
}

func (b *Buffer) arg(arg *ir.Arg) {
	b.spec(arg.Type)
	b.space()
	b.write(arg.Name)
}

func (b *Buffer) block(blk *ir.Block) {
	b.write("{")

	if len(blk.Nodes) != 0 {
		b.nl()
	}
	b.inc()

	for _, node := range blk.Nodes {
		b.idn()
		b.statement(node)
		b.nl()
	}

	b.dec()
	b.idn()
	b.write("}")
}

type Buffer struct {
	buf []byte

	// identation buffer, upon starting a new non-empty line
	// this buffer is used to add identation
	ib []byte

	// maps Type to its text representaion in C
	types map[*typ.Type]string

	// maps Symbol to its name in C output
	names map[*ir.Symbol]string
}

func NewBuffer(size int) *Buffer {
	return &Buffer{
		buf:   make([]byte, 0, size),
		types: make(map[*typ.Type]string),
		names: make(map[*ir.Symbol]string),
	}
}

func (b *Buffer) write(s string) {
	b.buf = append(b.buf, []byte(s)...)
}

func (b *Buffer) inc() {
	b.ib = append(b.ib, '\t')
}

func (b *Buffer) dec() {
	b.ib = b.ib[:len(b.ib)-1]
}

func (b *Buffer) comment(s string) {
	b.write("// ")
	b.write(s)
	b.nl()
}

func (b *Buffer) space() {
	b.write(" ")
}

func (b *Buffer) statement(node any) {
	switch s := node.(type) {
	case ir.ConstInit:
		b.spec(s.Sym.Type())
		b.space()
		b.write(s.Sym.Name)
		b.write(" = ")
		b.expr(s.Expr)
	case ir.Return:
		b.write("return")
		if s.Expr != nil {
			b.space()
			b.expr(s.Expr)
		}
	case ir.Execute:
		b.expr(s.Expr)
	case ir.Assign:
		b.write(b.lookupName(s.Sym))
		b.write(" = ")
		b.expr(s.Expr)
	case ir.VarInit:
		b.spec(s.Sym.Type())
		b.space()
		b.write(s.Sym.Name)
		b.write(" = ")
		b.expr(s.Expr)
	case ir.VarCreate:
		b.spec(s.Type)
		b.space()
		b.write(s.Sym.Name)
		b.write(" = ")
		b.expr(s.Type.Init)
	case ir.If:
		b.write("if (")
		b.expr(s.Condition)
		b.write(") ")
		b.block(&s.Block)
		return
	case ir.IfElse:
		b.write("if (")
		b.expr(s.Condition)
		b.write(") ")
		b.block(&s.Block)
		b.write(" else ")
		b.block(&s.Else)
		return
	case ir.ForEver:
		b.write("while (true) ")
		b.block(&s.Body)
		return
	case ir.While:
		b.write("while (")
		b.expr(s.Condition)
		b.write(") ")
		b.block(&s.Body)
		return
	case ir.AddAssign:
		b.write(b.lookupName(s.Sym))
		b.write(" += ")
		b.expr(s.Expr)
	case ir.VarDirty:
		b.spec(s.Type)
		b.space()
		b.write(s.Sym.Name)
	default:
		panic(fmt.Sprintf("not implemented for %T", s))
	}

	b.write(";")
}

func (b *Buffer) expr(expr any) {
	switch e := expr.(type) {
	case ir.Binary:
		b.binaryExpr(e)
	case ir.Unary:
		b.unaryExpr(e)
	default:
		b.operand(e)
	}
}

func (b *Buffer) unaryExpr(unary ir.Unary) {
	b.write(unary.Op.String())
	b.unaryOperand(unary.Operand)
}

func (b *Buffer) operand(operand any) {
	switch o := operand.(type) {
	case ir.Usage:
		b.write(b.lookupName(o.Symbol))
	case ir.True:
		b.write("true")
	case ir.False:
		b.write("false")
	case ir.Nil:
		b.write("nil")
	case ir.Integer:
		if o.Lit != "" {
			b.write(o.Lit)
		} else {
			b.write(strconv.FormatUint(o.Val, 10))
		}
	case ir.Str:
		b.write("\"")
		b.write(o.Lit)
		b.write("\"")
	case ir.Pares:
		b.write("(")
		b.expr(o.Inner)
		b.write(")")
	case ir.Call:
		b.call(o)
	default:
		panic(fmt.Sprintf("not implemented for %T", o))
	}
}

func (b *Buffer) call(call ir.Call) {
	switch target := call.Target.(type) {
	case *ir.Symbol:
		b.write(b.lookupName(target))
	case ir.Select:
		if target.Target.Type().Kind == typ.Unit {
			// import from other unit

			b.write(b.lookupName(target.Result.(*ir.Symbol)))
		} else {
			panic("not implemented")
		}
	default:
		panic(fmt.Sprintf("not implemented for %T", target))
	}
	b.tupleLiteral(call.Args)
}

// func (b *Buffer) selector(expr ast.SelectorExpression) {
// 	b.compound(expr.Target)
// 	b.write(".")
// 	b.write(expr.Selected.Lit)
// }

// func (b *Buffer) index(expr ast.IndexExpression) {
// 	b.compound(expr.Target)
// 	b.write("[")
// 	b.expr(expr.Index)
// 	b.write("]")
// }

func (b *Buffer) tupleLiteral(args []typ.Typed) {
	b.write("(")
	if len(args) != 0 {
		b.expr(args[0])
		for i := 1; i < len(args); i++ {
			b.write(", ")
			b.expr(args[i])
		}
	}
	b.write(")")
}

func (b *Buffer) unaryOperand(operand any) {
	switch o := operand.(type) {
	case ir.Unary:
		b.unaryExpr(o)
	default:
		b.operand(o)
	}
}

func (b *Buffer) binaryExpr(binary ir.Binary) {
	b.expr(binary.Left)
	b.space()
	b.write(binary.Op.String())
	b.space()
	b.expr(binary.Right)
}

// end current line
func (b *Buffer) nl() {
	b.write("\n")
}

// add identation
func (b *Buffer) idn() {
	b.buf = append(b.buf, b.ib...)
}
