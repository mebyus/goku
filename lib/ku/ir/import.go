package ir

import (
	"codeberg.org/mebyus/goku/lib/ku/ir/origin"
	"codeberg.org/mebyus/goku/lib/ku/source"
)

type Import struct {
	Pos source.Pos

	Path origin.Path

	// Local name for imported unit
	Name string

	// Unit which is being bound to local name by import
	Unit *Unit
}
