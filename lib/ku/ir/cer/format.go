package cer

import (
	"fmt"
	"strconv"
)

func (e *Error) Error() string {
	switch e.Kind {
	case UnexpectedToken:
		return fmt.Sprintf("[%04d] unexpected token \"%s\" at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case UnresolvedSymbol:
		return fmt.Sprintf("[%04d] unresolved symbol \"%s\" at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case MultipleSymbolDefinitions:
		return fmt.Sprintf("[%04d] multiple symbol \"%s\" definitions at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case NoExplicitInit:
		return fmt.Sprintf("[%04d] type \"%s\" does not have default initial value at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case ConditionTypeMismatch:
		return fmt.Sprintf("[%04d] condition must result in boolean value at %s", e.Kind, e.Span.Pin().String())
	case NotAssignableExpression:
		return fmt.Sprintf("[%04d] expression \"%s\" is not assignable at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case UnknownImportOrigin:
		return fmt.Sprintf("[%04d] unknown import origin \"%s\" at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case SameStringImport:
		return fmt.Sprintf("[%04d] reimport of unit \"%s\" at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case ImportCycle:
		return fmt.Sprintf("[%04d] units (%s) form import cycle", e.Kind, e.Msg)
	case EmptyImportString:
		return fmt.Sprintf("[%04d] empty import string at %s", e.Kind, e.Span.Pin().String())
	case BadImportString:
		return fmt.Sprintf("[%04d] bad import string \"%s\" at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case SelfImport:
		return fmt.Sprintf("[%04d] unit \"%s\" imports itself at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case MultipleSymbolDeclarations:
		return fmt.Sprintf("[%04d] multiple symbol \"%s\" declarations at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case OrphanedAtrBlock:
		return fmt.Sprintf("[%04d] orphaned attributes at %s", e.Kind, e.Span.Pin().String())
	case NotCallableSymbol:
		return fmt.Sprintf("[%04d] \"%s\" is not a function and cannot be called at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case TooManyArguments:
		return fmt.Sprintf("[%04d] too many arguments in function call (%s) at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case NotEnoughArguments:
		return fmt.Sprintf("[%04d] not enough arguments in function call (%s) at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case NotTypeSymbol:
		return fmt.Sprintf("[%04d] \"%s\" is not a type name at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case MultipleArgumentDeclarations:
		return fmt.Sprintf("[%04d] multiple argument \"%s\" declarations at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case NotFoundInUnit:
		return fmt.Sprintf("[%04d] %s at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case NotExportedSymbol:
		return fmt.Sprintf("[%04d] %s at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case EmptyUnit:
		return fmt.Sprintf("[%04d] unit \"%s\" does not contain symbols", e.Kind, e.Msg)
	case OperatorNotDefined:
		return fmt.Sprintf("[%04d] operator not defined on %s at %s", e.Kind, e.Msg, e.Span.Pin().String())
	case unspecified:
		panic("kind is unspecified")
	default:
		panic("not implemented for " + strconv.FormatInt(int64(e.Kind), 10))
	}
}
