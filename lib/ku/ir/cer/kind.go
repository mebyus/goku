// package cer contains various abstractions which represent compilation errors
// in Ku compiler
package cer

import (
	"codeberg.org/mebyus/goku/lib/ku/source"
)

type Kind int

const (
	// Zero value of Kind. Explicit usage is discouraged
	//
	// Upon encountering this value most functions and methods will
	// (and should) panic
	unspecified Kind = iota

	UnexpectedToken
	OrphanedAtrBlock

	UnknownImportOrigin
	EmptyImportString
	BadImportString
	SelfImport
	SameStringImport
	ImportCycle
	EmptyUnit

	UnresolvedSymbol
	NotFoundInUnit
	NotExportedSymbol
	MultipleSymbolDeclarations
	MultipleSymbolDefinitions
	MultipleArgumentDeclarations
	NoExplicitInit
	ConditionTypeMismatch
	NotAssignableExpression
	NotCallableSymbol
	TooManyArguments
	NotEnoughArguments
	NotTypeSymbol

	OperatorNotDefined
)

type Error struct {
	Span source.Span

	Msg string

	// Content and its meaning is determined by Kind
	Info any

	Kind Kind
}
