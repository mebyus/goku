package ir

import (
	"fmt"

	"codeberg.org/mebyus/goku/lib/ku/ast"
	"codeberg.org/mebyus/goku/lib/ku/ir/cer"
	"codeberg.org/mebyus/goku/lib/ku/ir/symbol"
	"codeberg.org/mebyus/goku/lib/ku/ir/typ"
)

// Tindex is index of program or unit types
type Tindex struct {
	// set of all Types
	set map[*typ.Type]struct{}

	// Maps a Type to a base Type chunk of given Type
	//
	// For example in pseudo-code:
	//
	//	chunks[u8] => []u8
	//
	// And if type "byte" is a Flavor of "u8" then:
	//
	//	chunks[byte] => []u8
	//	chunks[byte.Base (= u8)] => []u8
	//	base of []byte = []u8
	chunks map[*typ.Type]*typ.Type

	pointers map[*typ.Type]*typ.Type

	byUID map[typ.UID]*typ.Type

	// maps a base Type to a set of its Flavors
	flavors map[*typ.Type]map[*typ.Type]struct{}
}

func NewEmptyTindex() *Tindex {
	return &Tindex{
		set:      make(map[*typ.Type]struct{}),
		byUID:    make(map[typ.UID]*typ.Type),
		chunks:   make(map[*typ.Type]*typ.Type),
		pointers: make(map[*typ.Type]*typ.Type),
		flavors:  make(map[*typ.Type]map[*typ.Type]struct{}),
	}
}

func NewTindex() *Tindex {
	tx := NewEmptyTindex()

	tx.Add(typ.Void)
	tx.Add(typ.I)
	tx.Add(typ.ISZ)
	tx.Add(typ.F)
	tx.Add(typ.B)
	tx.Add(typ.S)
	tx.Add(typ.N)
	tx.Add(typ.U8)
	tx.Add(typ.I32)
	// tx.Add(typ.Bytes)
	tx.Add(typ.Str)

	return tx
}

func (tx *Tindex) Add(t *typ.Type) {
	tx.set[t] = struct{}{}
	flavors, ok := tx.flavors[t.Base]
	if !ok {
		flavors = make(map[*typ.Type]struct{})
		tx.flavors[t.Base] = flavors
	}
	flavors[t] = struct{}{}
}

func (tx *Tindex) List() []*typ.Type {
	list := make([]*typ.Type, 0, len(tx.set))
	for t := range tx.set {
		list = append(list, t)
	}
	return list
}

func FormatType(t *typ.Type) string {
	switch t.Kind {
	case typ.Named:
		return t.Name
	case typ.Unsigned, typ.Signed, typ.String, typ.Zero:
		return t.Name
	case
		typ.StaticBoolean, typ.StaticFloat, typ.StaticNil,
		typ.StaticString, typ.StaticInteger:

		return t.Kind.String()
	case typ.Chunk:
		return "[]" + FormatType(t.Def.(*typ.Type))
	default:
		panic(fmt.Sprintf("not implemented for %s", t.Kind.String()))
	}
}

// Spec processes type specifier to produce a Type. It returns already
// existing Type if similar type specifier was processed previously and
// stored inside Index
func (s *Scope) Spec(spec ast.TypeSpecifier) (*typ.Type, error) {
	switch p := spec.(type) {
	case nil:
		return typ.Void, nil
	case ast.Identifier:
		name := p.Lit
		sym := s.Find(name, p.Pos.Ofs)
		if sym == nil {
			return nil, &cer.Error{
				Kind: cer.UnresolvedSymbol,
				Span: p.Pos,
				Msg:  name,
			}
		}
		if sym.Kind != symbol.Type {
			return nil, &cer.Error{
				Kind: cer.NotTypeSymbol,
				Span: p.Pos,
				Msg:  name,
			}
		}
		return sym.Def.(*typ.Type), nil
	case ast.StructType:
		fields := make([]typ.Field, 0, len(p.Fields))
		for _, field := range p.Fields {
			t, err := s.Spec(field.Type)
			if err != nil {
				return nil, err
			}
			fields = append(fields, typ.Field{
				Name: field.Name.Lit,
				Type: t,
			})
		}
		def, err := typ.FromFields(fields)
		if err != nil {
			return nil, err
		}
		t := &typ.Type{
			Def:  def,
			Kind: typ.Struct,
			Unit: 0, // TODO: pipe Unit.Hash here
		}
		t.Base = t
		uid := t.UID()
		stored, ok := s.Types.byUID[uid]
		if !ok {
			s.Types.byUID[uid] = t
		} else {
			t = stored
		}
		return t, nil
	case ast.SliceTypeLiteral:
		elem, err := s.Spec(p.ElementType)
		if err != nil {
			return nil, err
		}
		if elem.IsBase() {
			base, ok := s.Types.chunks[elem]
			if !ok {
				base = &typ.Type{
					Def:  elem,
					Kind: typ.Chunk,
				}
				base.Base = base
				s.Types.chunks[elem] = base
				s.Types.Add(base)
			}
			return base, nil
		}

		panic("flavors search not implemented")

	// base, ok := s.Types.chunks[elem.Base]
	// if !ok {
	// 	base = &typ.Type{
	// 		Def:  elem.Base,
	// 		Kind: typ.Chunk,
	// 	}
	// 	base.Base = base
	// 	s.Types.chunks[elem.Base] = base
	// 	s.Types.Add(base)
	// }
	// // flavors := s.Types.flavors[base]
	// t := &typ.Type{
	// 	Def:  elem,
	// 	Base: base,
	// 	Kind: typ.Chunk,
	// }
	// _ = t
	case ast.UnnamedTuple:
		fields := make([]*typ.Type, 0, len(p))
		for _, field := range p {
			t, err := s.Spec(field)
			if err != nil {
				return nil, err
			}
			fields = append(fields, t)
		}
		def := typ.TupleFromFields(fields)
		t := &typ.Type{
			Def:  def,
			Kind: typ.Tuple,
			Unit: 0, // TODO: pipe Unit.Hash here
		}
		t.Base = t
		uid := t.UID()
		stored, ok := s.Types.byUID[uid]
		if !ok {
			s.Types.byUID[uid] = t
		} else {
			t = stored
		}
		return t, nil
	case ast.NamedTuple:
		fields := make([]typ.Field, 0, len(p))
		for _, field := range p {
			t, err := s.Spec(field.Type)
			if err != nil {
				return nil, err
			}
			fields = append(fields, typ.Field{
				Name: field.Name.Lit,
				Type: t,
			})
		}
		def, err := typ.FormationFromFields(fields)
		if err != nil {
			return nil, err
		}
		t := &typ.Type{
			Def:  def,
			Kind: typ.Formation,
			Unit: 0, // TODO: pipe Unit.Hash here
		}
		t.Base = t
		uid := t.UID()
		stored, ok := s.Types.byUID[uid]
		if !ok {
			s.Types.byUID[uid] = t
		} else {
			t = stored
		}
		return t, nil
	case ast.PointerTypeLiteral:
		derefType, err := s.Spec(p.BaseType)
		if err != nil {
			return nil, err
		}
		if derefType.IsBase() {
			refType, ok := s.Types.pointers[derefType]
			if !ok {
				refType = &typ.Type{
					Def:  derefType,
					Kind: typ.Pointer,
				}
				refType.Base = refType
				s.Types.pointers[derefType] = refType
				s.Types.Add(refType)
			}
			return refType, nil
		}

		panic("flavors search not implemented")
	default:
		panic(fmt.Sprintf("not implemented for %T", p))
	}
}
