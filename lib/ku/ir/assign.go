package ir

import "codeberg.org/mebyus/goku/lib/ku/ir/typ"

// Assign statement assigns a new value to existing variable
type Assign struct {
	// Expression Node
	Expr any

	Sym *Symbol
}

// AddAssign statement adds a value to existing variable
type AddAssign struct {
	// Expression Node
	Expr any

	Sym *Symbol
}

// ConstInit statement creates new constant and assigns value to it
type ConstInit struct {
	// Expression Node
	Expr any

	Sym *Symbol
}

type TypedConst struct {
	// Expression Node
	Expr typ.Typed

	// Declared const type
	Type *typ.Type

	Sym *Symbol
}

// VarInit statement creates new variable and assigns value to it
type VarInit struct {
	// Expression Node
	Expr any

	Sym *Symbol
}

// VarCreate statement creates new variable (with specified type),
// but does not assign an explicit initial value to it
type VarCreate struct {
	Type *typ.Type

	Sym *Symbol
}

// VarDirty statement creates new variable (with specified type),
// and explicitly skips default initialization
type VarDirty struct {
	Type *typ.Type

	Sym *Symbol
}
