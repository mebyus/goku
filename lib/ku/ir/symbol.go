package ir

import (
	"codeberg.org/mebyus/goku/lib/ku/ir/symbol"
	"codeberg.org/mebyus/goku/lib/ku/ir/typ"
	"codeberg.org/mebyus/goku/lib/ku/source"
)

// Symbol represents an Identifier definition (builtin or user-defined)
type Symbol struct {
	// Position where symbol is defined
	Pos source.Pos

	Name string

	// Symbol definition. Meaning and content of this field depend
	// on Kind value
	//
	//	- Import: *Unit struct (imported unit)
	//	- Type: *typ.Type struct
	//	- Function: *Fn struct
	//	- Argument: *Arg struct
	//
	Def any

	Scope *Scope

	Unit *Unit

	// Type of Symbol usage result
	typ.With

	UsageCount uint32

	Kind symbol.Kind

	Public bool
}
