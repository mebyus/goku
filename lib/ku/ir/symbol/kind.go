package symbol

type Kind uint8

const (
	unspecified Kind = iota

	Import
	Type
	Fn
	Proto
	Blue
	Const
	Var
	Arg
)

var text = [...]string{
	unspecified: "<unspecified>",

	Import: "import",
	Type:   "type",
	Fn:     "fn",
	Proto:  "proto",
	Blue:   "blue",
	Const:  "const",
	Var:    "var",
	Arg:    "arg",
}

func (k Kind) String() string {
	return text[k]
}
