package ir

type Program struct {
	EntryPoint *Symbol

	// Only includes Types which are builtin or exposed by Units via "pub" keyword
	Types *Tindex
}
