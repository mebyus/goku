package ir

import (
	"fmt"

	"codeberg.org/mebyus/goku/lib/ku/ast"
	"codeberg.org/mebyus/goku/lib/ku/ir/cer"
	"codeberg.org/mebyus/goku/lib/ku/ir/scope"
	"codeberg.org/mebyus/goku/lib/ku/ir/symbol"
	"codeberg.org/mebyus/goku/lib/ku/ir/typ"
)

type Block struct {
	// Name of the file that contains this Block
	// File string

	// Each Node represents a statement in Block
	Nodes []any

	Scope *Scope
}

func (b *Block) fill(stmts []ast.Statement, refs map[*Symbol]struct{}) error {
	for _, stmt := range stmts {
		err := b.add(stmt, refs)
		if err != nil {
			return err
		}
	}
	return nil
}

func (b *Block) add(stmt ast.Statement, refs map[*Symbol]struct{}) error {
	switch s := stmt.(type) {
	case ast.ShortAssign:
		name := s.Name.Lit
		sym := b.Scope.Sym(name)
		if sym != nil {
			return &cer.Error{
				Span: s.Name.Pos,
				Msg:  name,
				Kind: cer.MultipleSymbolDefinitions,
			}
		}
		expr, err := b.Scope.expr(s.Expression, refs)
		if err != nil {
			return err
		}
		sym = &Symbol{
			Name: name,
			Kind: symbol.Const,
			Pos:  s.Name.Pos,
			Def:  expr,
			With: typ.Derive(expr),
		}
		b.Scope.Add(sym)
		b.Nodes = append(b.Nodes, ConstInit{
			Sym:  sym,
			Expr: expr,
		})
	case ast.TypedAssign:
		name := s.Name.Lit
		sym := b.Scope.Sym(name)
		if sym != nil {
			return &cer.Error{
				Span: s.Name.Pos,
				Msg:  name,
				Kind: cer.MultipleSymbolDefinitions,
			}
		}
		t, err := b.Scope.Spec(s.Type)
		if err != nil {
			return err
		}
		expr, err := b.Scope.expr(s.Expression, refs)
		if err != nil {
			return err
		}
		// TODO: add type check between declared type and expression
		sym = &Symbol{
			Name: name,
			Kind: symbol.Const,
			Pos:  s.Name.Pos,
		}
		b.Scope.Add(sym)
		b.Nodes = append(b.Nodes, TypedConst{
			Expr: expr,
			Type: t,
			Sym:  sym,
		})
	case ast.VarDeclaration:
		name := s.Name.Lit
		sym := b.Scope.Sym(name)
		if sym != nil {
			return &cer.Error{
				Span: s.Name.Pos,
				Msg:  name,
				Kind: cer.MultipleSymbolDefinitions,
			}
		}
		t, err := b.Scope.Spec(s.Type)
		if err != nil {
			return err
		}
		if t.Init == nil {
			return &cer.Error{
				Span: s.Name.Pos,
				Msg:  t.Kind.String(),
				Kind: cer.NoExplicitInit,
			}
		}
		sym = &Symbol{
			Name: name,
			With: typ.With{Typ: t},
			Kind: symbol.Var,
			Pos:  s.Name.Pos,
		}
		b.Scope.Add(sym)
		b.Nodes = append(b.Nodes, VarCreate{
			Type: t,
			Sym:  sym,
		})
	case ast.VarShortAssign:
		name := s.Name.Lit
		sym := b.Scope.Sym(name)
		if sym != nil {
			return &cer.Error{
				Span: s.Name.Pos,
				Msg:  name,
				Kind: cer.MultipleSymbolDefinitions,
			}
		}
		expr, err := b.Scope.expr(s.Expression, refs)
		if err != nil {
			return err
		}
		sym = &Symbol{
			Name: name,
			Kind: symbol.Var,
			Pos:  s.Name.Pos,
			Def:  expr,
			With: typ.Derive(expr),
		}
		b.Scope.Add(sym)
		b.Nodes = append(b.Nodes, VarInit{
			Sym:  sym,
			Expr: expr,
		})
	case ast.VarDefinition:
		name := s.Declaration.Name.Lit
		sym := b.Scope.Sym(name)
		if sym != nil {
			return &cer.Error{
				Span: s.Declaration.Name.Pos,
				Msg:  name,
				Kind: cer.MultipleSymbolDefinitions,
			}
		}
		t, err := b.Scope.Spec(s.Declaration.Type)
		if err != nil {
			return err
		}
		expr, err := b.Scope.expr(s.Expression, refs)
		if err != nil {
			return err
		}
		// TODO: add type check between declared type and expression
		sym = &Symbol{
			Name: name,
			With: typ.With{Typ: t},
			Kind: symbol.Var,
			Pos:  s.Declaration.Name.Pos,
			Def:  expr,
		}
		b.Scope.Add(sym)
		b.Nodes = append(b.Nodes, VarInit{
			Sym:  sym,
			Expr: expr,
		})
	case ast.VarDirty:
		name := s.Name.Lit
		sym := b.Scope.Sym(name)
		if sym != nil {
			return &cer.Error{
				Span: s.Name.Pos,
				Msg:  name,
				Kind: cer.MultipleSymbolDefinitions,
			}
		}
		t, err := b.Scope.Spec(s.Type)
		if err != nil {
			return err
		}
		sym = &Symbol{
			Name: name,
			Kind: symbol.Var,
			With: typ.With{Typ: t},
			Pos:  s.Name.Pos,
		}
		b.Scope.Add(sym)
		b.Nodes = append(b.Nodes, VarDirty{
			Type: t,
			Sym:  sym,
		})
	case ast.IfStatement:
		condition, err := b.Scope.expr(s.If.Condition, refs)
		if err != nil {
			return err
		}
		t := condition.Type()
		if t.Base.Kind != typ.Boolean && t.Base.Kind != typ.StaticBoolean {
			return &cer.Error{
				Span: s.If.Body.Pos,
				Kind: cer.ConditionTypeMismatch,
			}
		}
		ifBlock := Block{
			Scope: &Scope{
				Names:  make(map[string]*Symbol),
				Parent: b.Scope,
				Kind:   scope.If,
				Pos:    s.If.Body.Pos.Ofs,
			},
		}
		err = ifBlock.fill(s.If.Body.Statements, refs)
		if err != nil {
			return err
		}
		if s.Else == nil {
			b.Nodes = append(b.Nodes, If{
				Condition: condition,
				Block:     ifBlock,
			})
			return nil
		}
		elseBlock := Block{
			Scope: &Scope{
				Names:  make(map[string]*Symbol),
				Parent: b.Scope,
				Kind:   scope.Else,
				Pos:    s.Else.Body.Pos.Ofs,
			},
		}
		err = elseBlock.fill(s.Else.Body.Statements, refs)
		if err != nil {
			return err
		}
		b.Nodes = append(b.Nodes, IfElse{
			If: If{
				Condition: condition,
				Block:     ifBlock,
			},
			Else: elseBlock,
		})
	case ast.ForEver:
		forEver := ForEver{
			Body: Block{
				Scope: &Scope{
					Names:  make(map[string]*Symbol),
					Parent: b.Scope,
					Kind:   scope.Loop,
					Pos:    s.Body.Pos.Ofs,
				},
			},
		}
		err := forEver.Body.fill(s.Body.Statements, refs)
		if err != nil {
			return err
		}
		b.Nodes = append(b.Nodes, forEver)
	case ast.While:
		condition, err := b.Scope.expr(s.Condition, refs)
		if err != nil {
			return err
		}
		t := condition.Type()
		if t.Base.Kind != typ.Boolean && t.Base.Kind != typ.StaticBoolean {
			return &cer.Error{
				Span: s.Body.Pos,
				Kind: cer.ConditionTypeMismatch,
			}
		}
		while := While{
			Condition: condition,
			Body: Block{
				Scope: &Scope{
					Names:  make(map[string]*Symbol),
					Parent: b.Scope,
					Kind:   scope.Loop,
					Pos:    s.Body.Pos.Ofs,
				},
			},
		}
		err = while.Body.fill(s.Body.Statements, refs)
		if err != nil {
			return err
		}
		b.Nodes = append(b.Nodes, while)
	case ast.ExpressionStatement:
		expr, err := b.Scope.expr(s.Expression, refs)
		if err != nil {
			return err
		}

		b.Nodes = append(b.Nodes, Execute{Expr: expr})
	case ast.ReturnStatement:
		expr, err := b.Scope.expr(s.Expression, refs)
		if err != nil {
			return err
		}

		b.Nodes = append(b.Nodes, Return{Expr: expr})
	case ast.Assign:
		name := s.Target.Lit
		sym := b.Scope.Find(name, s.Target.Pos.Ofs)
		if sym == nil {
			return &cer.Error{
				Span: s.Target.Pos,
				Msg:  name,
				Kind: cer.UnresolvedSymbol,
			}
		}
		if sym.Kind != symbol.Var {
			return &cer.Error{
				Span: s.Target.Pos,
				Msg:  s.Target.Lit,
				Kind: cer.NotAssignableExpression,
			}
		}
		expr, err := b.Scope.expr(s.Expression, refs)
		if err != nil {
			return err
		}
		// TODO: add typecheck for variable and expression compatibility
		b.Nodes = append(b.Nodes, Assign{
			Sym:  sym,
			Expr: expr,
		})
	case ast.AddAssign:
		name := s.Target.Lit
		sym := b.Scope.Find(name, s.Target.Pos.Ofs)
		if sym == nil {
			return &cer.Error{
				Span: s.Target.Pos,
				Msg:  name,
				Kind: cer.UnresolvedSymbol,
			}
		}
		if sym.Kind != symbol.Var {
			return &cer.Error{
				Span: s.Target.Pos,
				Msg:  s.Target.Lit,
				Kind: cer.NotAssignableExpression,
			}
		}
		expr, err := b.Scope.expr(s.Expression, refs)
		if err != nil {
			return err
		}
		// TODO: add typecheck for variable and expression compatibility
		b.Nodes = append(b.Nodes, AddAssign{
			Sym:  sym,
			Expr: expr,
		})
	default:
		panic(fmt.Sprintf("not implemented for %T", s))
	}
	return nil
}
