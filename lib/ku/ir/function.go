package ir

import (
	"codeberg.org/mebyus/goku/lib/ku/ast"
	"codeberg.org/mebyus/goku/lib/ku/ir/cer"
	"codeberg.org/mebyus/goku/lib/ku/ir/scope"
	"codeberg.org/mebyus/goku/lib/ku/ir/symbol"
	"codeberg.org/mebyus/goku/lib/ku/ir/typ"
)

// Fn is a function defenition at IR level
type Fn struct {
	Name string

	// function arguments, equals nil if function has no arguments
	Args []*Arg

	// list of top-level package symbols which are used in function
	Refs []*Symbol

	Body Block

	// function return type
	Type *typ.Type
}

// Arg is an argument (of a function) definition at IR level
type Arg struct {
	Name string

	Type *typ.Type

	// Symbol defined by this argument
	Symbol *Symbol
}

func FnDef(parent *Scope, def ast.FunctionDefinition) (*Fn, error) {
	scope := &Scope{
		Names:  make(map[string]*Symbol),
		Parent: parent,
		Types:  parent.Types,
		Kind:   scope.Top,
	}

	var fnArgs []*Arg
	// TODO: simplify arguments processing for top-level functions
	// because they were already parsed during first unit indexing pass
	parameters := def.Declaration.Signature.Parameters
	if parameters != nil {
		args := parameters.(ast.NamedTuple)
		for _, arg := range args {
			name := arg.Name.Lit
			sym := scope.Sym(name)
			if sym != nil {
				return nil, &cer.Error{
					Kind: cer.MultipleArgumentDeclarations,
					Span: arg.Name.Pos,
					Msg:  name,
				}
			}

			t, err := parent.Spec(arg.Type)
			if err != nil {
				return nil, err
			}

			sym = &Symbol{
				With: typ.With{Typ: t},
				Name: name,
				Kind: symbol.Arg,
			}
			fnArg := &Arg{
				Name:   name,
				Symbol: sym,
				Type:   t,
			}
			sym.Def = fnArg

			scope.Add(sym)
			fnArgs = append(fnArgs, fnArg)
		}
	}

	fn := &Fn{
		Name: def.Declaration.Name.Lit,
		Args: fnArgs,
		Body: Block{
			Scope: scope,
		},
	}

	// for gathering function Refs
	refs := make(map[*Symbol]struct{})

	err := fn.Body.fill(def.Body.Statements, refs)
	if err != nil {
		return nil, err
	}

	if len(refs) != 0 {
		fn.Refs = make([]*Symbol, 0, len(refs))
		for r := range refs {
			fn.Refs = append(fn.Refs, r)
		}
	}

	return fn, nil
}
