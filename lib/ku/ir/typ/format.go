package typ

var textKind = [...]string{
	Zero:          "zero",
	StaticInteger: "static_integer",
	StaticFloat:   "static_float",
	StaticString:  "static_string",
	StaticBoolean: "static_boolean",
	StaticNil:     "static_nil",
	Named:         "named",
	Struct:        "struct",
	Tuple:         "tuple",
	Formation:     "formation",
	Array:         "array",
	Pointer:       "pointer",
	Enum:          "enum",
	Function:      "function",
	Spec:          "spec",
	Proto:         "proto",
	Blue:          "blue",
	Chunk:         "chunk",
	Unsigned:      "unsigned",
	Signed:        "signed",
	Float:         "float",
	String:        "string",
	Boolean:       "boolean",
	Nillable:      "nillable",
}

func (k Kind) String() string {
	return textKind[k]
}
