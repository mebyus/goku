package typ

var Void = &Type{
	Name: "void",
	Kind: Zero,
}

var I = &Type{
	Kind: StaticInteger,
}

var F = &Type{
	Kind: StaticFloat,
}

var B = &Type{
	Kind: StaticBoolean,
}

var S = &Type{
	Kind: StaticString,
}

var N = &Type{
	Kind: StaticNil,
}

var U8 = &Type{
	Name: "u8",
	Def:  8,
	Kind: Unsigned,
}

var U32 = &Type{
	Name: "u32",
	Def:  32,
	Kind: Unsigned,
}

var U64 = &Type{
	Name: "u64",
	Def:  64,
	Kind: Unsigned,
}

var USZ = &Type{
	Name: "usz",
	Def:  64,
	Kind: Unsigned,
}

var I32 = &Type{
	Name: "i32",
	Def:  32,
	Kind: Signed,
}

var I64 = &Type{
	Name: "i64",
	Def:  64,
	Kind: Signed,
}

var ISZ = &Type{
	Name: "isz",
	Def:  64,
	Kind: Signed,
}

var Str = &Type{
	Name: "str",
	Kind: String,
}

var Bool = &Type{
	Name: "bool",
	Kind: Boolean,
}

var Bytes = &Type{
	Def:  U8,
	Kind: Chunk,
}

// Spec which can hold any type
var Wildcard = &Type{
	Name: "Type",
	Def:  func(*Type) bool { return true },
	Kind: Spec,
}

var Ordered = &Type{
	Name: "Ordered",
	Def: func(t *Type) bool {
		k := t.Base.Kind
		return k == Unsigned || k == Signed || k == Float
	},
	Kind: Spec,
}

func init() {
	Void.Base = Void
	Void.Hash()

	I.Base = I
	F.Base = F
	B.Base = B
	S.Base = S
	N.Base = N

	U8.Base = U8
	U8.UID()

	U32.Base = U32
	U32.UID()

	U64.Base = U64
	U64.UID()

	USZ.Base = USZ
	USZ.UID()

	I32.Base = I32
	I32.UID()

	I64.Base = I64
	I64.UID()

	ISZ.Base = ISZ
	ISZ.UID()

	Str.Base = Str
	Str.UID()

	Bool.Base = Bool
	Bool.UID()

	Bytes.Base = Bytes

	Wildcard.Base = Wildcard
	Ordered.Base = Ordered
}
