package typ

import (
	"encoding/binary"
	"fmt"
	"hash/fnv"
)

func HashName(name string) uint64 {
	h := fnv.New64a()
	h.Write([]byte(name))
	return h.Sum64()
}

func (t *Type) Hash() uint64 {
	if t.Stable != 0 {
		return t.Stable
	}
	t.Stable = t.hash()
	if t.Stable == 0 {
		panic("hashing a type should not produce 0")
	}
	return t.Stable
}

func (t *Type) hash() uint64 {
	switch t.Kind {
	case Zero, Signed, Unsigned, Float, String, Boolean:
		return HashName(t.Name)
	case Named:
		return HashName(t.Name)
	case Struct:
		return HashFields(t.Def.(StructSpec).Fields)
	case Chunk:
		return HashChunk(t.Def.(*Type))
	case Pointer:
		return t.Def.(*Type).Hash()
	case Tuple:
		return HashTypeList(t.Def.(TupleSpec).Fields)
	case Formation:
		return HashFields(t.Def.(FormationSpec).Fields)
	default:
		panic(fmt.Sprintf("not implemented for %s", t.Kind.String()))
	}
}

func HashTypeList(list []*Type) uint64 {
	var buf [8]byte

	h := fnv.New64a()
	for _, t := range list {
		binary.LittleEndian.PutUint64(buf[:], t.Hash())
		h.Write(buf[:])
	}
	return h.Sum64()
}

func HashFields(fields []Field) uint64 {
	var buf [8]byte

	h := fnv.New64a()
	for _, field := range fields {
		h.Write([]byte(field.Name))
		binary.LittleEndian.PutUint64(buf[:], field.Type.Hash())
		h.Write(buf[:])
	}
	return h.Sum64()
}

// HashChunk returns hash of []T type for a given T type
func HashChunk(t *Type) uint64 {
	var buf [8]byte

	h := fnv.New64a()
	binary.LittleEndian.PutUint64(buf[:], uint64(Chunk))
	h.Write(buf[:])
	binary.LittleEndian.PutUint64(buf[:], t.Hash())
	h.Write(buf[:])
	return h.Sum64()
}
