package typ

// With is a convenience struct for embedding into other structs
// therefore providing simple implementation of Typed interface
type With struct {
	Typ *Type
}

func (w With) Type() *Type {
	return w.Typ
}

func Derive(typed Typed) With {
	return With{Typ: typed.Type()}
}
