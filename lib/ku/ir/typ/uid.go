package typ

// UID is a unique identifier of a type inside a program. Via this identifier
// different *Type instances (with different pointers) can be compared to
// establish identity of represented types
//
// Main scenario for UID usage is as follows:
//
//  1. Encounter TypeSpecifier during AST traversal
//  2. Create fresh &Type{} struct and populate it with data according to TypeSpecifier
//  3. Compute Type.UID
//  4. Search UID lookup table for already created identical type
//  5. Add new UID entry to lookup table if no existing type was found
type UID struct {
	// equal to Stable hash field in Type struct
	h uint64

	// equal to Stable hash field in Type.Base struct
	bh uint64

	// hash of unit in which type is defined (if it's a Named type)
	uh uint64

	// kind of Type itself
	k Kind

	// kind of Type.Base
	bk Kind
}

func (t *Type) UID() UID {
	return UID{
		h:  t.Hash(),
		bh: t.Base.Hash(),
		uh: t.Unit,
		k:  t.Kind,
		bk: t.Base.Kind,
	}
}
