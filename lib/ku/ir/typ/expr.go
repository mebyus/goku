package typ

// Purity determines dynamic/static nature of expression or function
type Purity uint8

const (
	// Value known at compile-time
	Static Purity = iota

	// Can be computed without side-effects
	Pure

	// Computation causes side-effects
	Impure
)

type Dyn interface {
	Purity() Purity
}

type Expr interface {
	Typed
	Dyn
}
