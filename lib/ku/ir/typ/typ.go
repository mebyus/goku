package typ

import (
	"fmt"
	"sort"
)

// Describes Type definition category
type Kind uint8

const (
	// Zero size Type. There is always only one Type of this category and
	// it is the Base of all zero size Types
	Zero Kind = iota

	// Type for static integers (literals, constants and exressions).
	// Includes positive, negative integers and zero
	//
	// There is always only one Type of this category and it is the Base
	// of all static positive integer Flavors
	StaticInteger

	// Type for static floats (literals, constants and exressions)
	//
	// There is always only one Type of this category and it cannot
	// have Flavors
	StaticFloat

	// Type for static strings (literals, constants and exressions)
	//
	// There is always only one Type of this category and it cannot
	// have Flavors
	StaticString

	// Type for static boolean values (literals, constants and exressions)
	//
	// There is always only one Type of this category and it cannot
	// have Flavors
	StaticBoolean

	// Type for static nil value (literal, constants and exressions)
	//
	// There is always only one Type of this category and it cannot
	// have Flavors
	StaticNil

	// Types of symbols, created by importing other unit
	//
	//	import std {
	//		math => "math"
	//	}
	//
	// Symbol math in the example above has Type with Kind = Unit.
	// each distinct unit there will be each own Base type which
	// equals this one
	Unit

	// Types which are defined by giving a new Name to another Type
	//
	// Most commonly created via language construct:
	//
	//	type Name <OtherType>
	Named

	// Types which are defined as structs
	//
	// Specified via language construct:
	//
	//	struct { <Fields> }
	Struct

	// Types which are defined as tuples
	//
	// Specified via language construct:
	//
	//	(<type_1>, <type_2>, ...) - for unnamed tuples
	//	(<name_1>: <type_1>, <name_2>: <type_2>, ...) - for named tuples
	//
	// Tuples can only be used for specifying return values of functions and
	// cannot be named (or have Flavors through other means). Thus users of the
	// language cannot define their own tuple types with unique names
	//
	// Tuples are "behind-the-scenes" mechanism for implementing functions
	// with multiple return values and passing arguments from one function to
	// another in constructs like these:
	//
	//	foo(bar())
	//
	// or like these:
	//
	//	r := bar()
	//	foo(r)
	//
	// where function signatures of foo and bar are defined (for example) as:
	//
	//	fn bar() => (str, usz)
	//	fn foo(str, usz)
	//
	// Our reasoning for such restricted usage of tuples in the language are
	// mostly as follows:
	//
	// 1. For named tuples there are already structs present in the language,
	// no reason to multiply confusion by adding the same functionality in
	// different wrapping
	//
	// 2. Unnamed tuples should be treated as short-lived opaque containers and
	// unpacked into constants/variables with appropriate naming or handed over to
	// another function. There is no reason to introduce language constructs like these:
	//
	//	r := bar()
	//	first := r.0
	//
	// Accessing individual fields of unnamed tuples via "dot" syntax with numerical
	// "field" names is just confusing and should not be allowed. Instead programmers
	// are encouraged to use either named return values to clarify meaning of said values
	// or a struct to begin with, which is much easier for field naming and leaving
	// comments to those fields
	//
	// In short:
	//
	// Structs as named types are superior to tuples in all practical ways and thus
	// tuple serve a very niche purpose of passing around and unpacking returned values
	// from a function
	//
	// In case of named return values (named tuples) rules are not so strict. Individual
	// fields can be accessed via their names:
	//
	//	fn bar() => (my_string: str, count: usz)
	//
	//	r := bar()
	//	s := r.my_string
	//	c := r.count
	Tuple

	// Types which are defined as named tuples
	// TODO: consider renaming concept "named tuples" to "formations" across the project
	//
	// See comment to Tuple for more info
	Formation

	// Types which are defined as continuous region of memory holding
	// fixed number of elements of another Type
	//
	//	[5]u64 - an array of five u64 numbers
	Array

	// Pointer to another Type. An example of such a Type could be *str
	// (pointer to a string)
	Pointer

	// Abstract enum Types. Values of this Types can only be obtained via
	// their enum respective symbols. Furthermore enum Flavors cannot be
	// cast into integer Flavors
	Enum

	// Types which are defined as function signatures
	//
	// Specified via language construct:
	//
	//	fn (<Args>) <Result>
	Function

	// Types which are obtained via usage of type specifiers. In other
	// words this category contains Types of Types. For example:
	//
	//	- []u8: yields a type specifier for chunk of bytes
	//	- string: yields a type specifier for string
	//
	// Type of these specifiers is also a Type with Kind = Spec.
	//
	// Most common usage for Types from this category is narrowing type
	// parameters in Prototypes and Blueprints
	Spec

	// Parameterized Prototype (mechanism for generic types)
	Proto

	// Parameterized Blueprint (mechanism for generic functions and methods)
	Blue

	// Chunk of another Type. An example of such a Type could be []u8
	// (chunk of u8 integers), which is commonly used to represent
	// raw bytes of data
	Chunk

	// Types with fixed bit size which can hold unsigned integers:
	//
	//	u8, u16, u32, u64
	Unsigned

	// Types with fixed bit size which can hold signed integers:
	//
	//	i8, i16, i32, i64
	Signed

	// Types with fixed bit size which can hold floating point numbers:
	//
	//	f32, f64
	Float

	// Builtin type which can hold fixed number of bytes. Inner structure
	// of this type is identical to chunk of bytes []u8. The difference
	// between these two types is logical, in most cases strings should
	// hold utf-8 encoded text opposed to arbitrary byte sequence in
	// chunk of bytes. However this is not a rule and not enforced by the
	// language in any way. Strings and chunks of bytes can be cast between
	// each other freely with no runtime overhead
	String

	// Builtin type which can hold one of two mutually exclusive values:
	//
	//	- true
	//	- false
	//
	// There is always only one Type of this category and it is the Base
	// of all boolean Flavors
	//
	// This Type is not a Flavor of any Integer Type. Furthermore Boolean
	// Flavors cannot be cast into integer Flavors
	Boolean

	// Types which can have nil value at runtime
	Nillable
)

// Type represents a value type of symbol, field, expression or subexpression
// in a program. In other words when something is used in the program source code
// Type describes what kind of result that usage yields
type Type struct {
	// Not all types have a Name. For example []i32 is just a chunk of sized
	// i32 numbers with no specific Name for that type
	Name string

	// Meaning of this field heavily depends on the Kind of this Type
	//
	//	- Zero: always nil
	//	- StaticInteger: always nil
	//	- StaticFloat: always nil
	//	- StaticString: always nil
	//	- StaticBoolean: always nil
	//	- StaticNil: always nil
	//	- Import: pointer to ir.Unit struct
	//	- Named: always nil
	//	- Struct: StructSpec struct
	//	- Tuple: TupleSpec struct
	//	- Formation: FormationSpec struct
	//	- Array: ArraySpec struct
	//	- Pointer: pointer to addressed Type
	//	- Function: Signature struct
	//	- Spec: (?) function which accepts a Type
	//	- Chunk: pointer to element's Type
	//	- Unsigned: int holding type size in bits
	//	- Signed: int holding type size in bits
	//	- Float: int holding type size in bits
	//	- String: always nil
	//	- Boolean: always nil
	//	- Nillable: pointer to decorated Type
	Def any

	// Default initial value of this Type. Equals nil if Type does not
	// have default initial value. In latter case initial value can be assigned
	// to variables of this Type only explicitly
	Init Expr

	// Pseudo-unique id (hash code) of the type. Depends only on type definition
	// of represented type and (maybe) compiler version. One should use this field
	// in conjunction with Kind to reduce collision chance of different types
	//
	// Does not depend upon:
	//	- position in source code
	//	- formatting
	//	- compiler run (stays the same across different builds)
	Stable uint64

	// Each Type has a Base. Base is a Type representing the raw definition (struct,
	// enum, union) and memory layout of a Type
	//
	// Types which has the same Base are called Flavors of that Base or Type (Base is always
	// a Flavor of itself by definition). Flavors can be cast among each other via
	// explicit type casts and under the hood such casts do not alter memory or
	// perform any operation. One and only one Base exists among all of its Flavors.
	// From formal mathematical point of view all Flavors of specific Type form
	// equivalence class in set of all Types in a program
	Base *Type

	// Not 0 only for user-defined (not builtin) Named Types. Contains hash id of a Unit
	// where the type is defined
	Unit uint64

	// Describes Type definition category. For raw definition check Base
	Kind Kind
}

// IsStatic checks if a given type is one of the static types
func (t *Type) IsStatic() bool {
	switch t.Kind {
	case
		StaticInteger, StaticFloat, StaticString,
		StaticBoolean, StaticNil:

		return true
	default:
		return false
	}
}

// Typed is an interface of something that has a Type
type Typed interface {
	Type() *Type
}

// ArraySpec defines array type
type ArraySpec struct {
	// Type of individual element in array
	Elem *Type

	Len uint
}

// SpecFunc can test whether or not a given Type satisfies a Spec
type SpecFunc func(*Type) bool

func (t *Type) IsBase() bool {
	return t.Base == t
}

// Signature defines function type
type Signature struct {
	// Required arg types to invoke a function
	Args []*Type

	// Returned result type. Always nil if Never = true
	Result *Type

	// True for functions which never return. Result field
	// will be nil in such cases
	Never bool
}

// Field represents a field in a struct Type
type Field struct {
	Name string
	Type *Type
}

type StructSpec struct {
	// sorted by field name
	Fields []Field

	// maps field name to its index in Fields slice
	byName map[string]int
}

// FromFields constructs a StructSpec from a given list of its fields.
// Takes ownership of the given slice
func FromFields(fields []Field) (StructSpec, error) {
	sort.Slice(fields, func(i, j int) bool {
		return fields[i].Name < fields[j].Name
	})
	s := StructSpec{
		Fields: fields,
		byName: make(map[string]int, len(fields)),
	}
	for i, field := range fields {
		_, ok := s.byName[field.Name]
		if ok {
			return StructSpec{}, fmt.Errorf("non-unique field name \"%s\" in struct", field.Name)
		}
		s.byName[field.Name] = i
	}
	return s, nil
}

func (s StructSpec) Field(name string) (Field, bool) {
	i, ok := s.byName[name]
	if !ok {
		return Field{}, false
	}
	return s.Fields[i], true
}

type FormationSpec struct {
	// sorted by original field order in formation
	Fields []Field

	// maps field name to its index in Fields slice
	byName map[string]int
}

// FormationFromFields constructs a FormationSpec from a given list of its fields.
// Takes ownership of the given slice
func FormationFromFields(fields []Field) (FormationSpec, error) {
	s := FormationSpec{
		Fields: fields,
		byName: make(map[string]int, len(fields)),
	}
	for i, field := range fields {
		_, ok := s.byName[field.Name]
		if ok {
			return FormationSpec{}, fmt.Errorf("non-unique field name \"%s\" in formation", field.Name)
		}
		s.byName[field.Name] = i
	}
	return s, nil
}

func (s FormationSpec) Field(name string) (Field, bool) {
	i, ok := s.byName[name]
	if !ok {
		return Field{}, false
	}
	return s.Fields[i], true
}

type TupleSpec struct {
	// sorted by original field order in tuple
	Fields []*Type
}

func TupleFromFields(fields []*Type) TupleSpec {
	return TupleSpec{
		Fields: fields,
	}
}
