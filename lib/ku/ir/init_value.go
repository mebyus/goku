package ir

import "codeberg.org/mebyus/goku/lib/ku/ir/typ"

func init() {
	typ.U8.Init = Integer{}
	typ.U32.Init = Integer{}
	typ.U64.Init = Integer{}
	typ.USZ.Init = Integer{}
	typ.I32.Init = Integer{}
	typ.I64.Init = Integer{}
	typ.ISZ.Init = Integer{}
	typ.Bool.Init = False{}
}
