package scope

type Kind uint8

const (
	unknown Kind = iota

	// Scope with global program-wide access. Holds builtin language symbols
	Global

	// Scope with unit-wide access (across all unit files)
	Unit

	// Scope created by top-level function
	Top

	// Scope created by function literal (with or without closure).
	Fn

	Block
	If
	Else
	Loop
)
