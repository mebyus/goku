package ir

type If struct {
	// Expression Node
	Condition any

	Block Block
}

type IfElse struct {
	If

	Else Block
}
