package ir

import (
	"fmt"

	"codeberg.org/mebyus/goku/lib/ku/ast"
	"codeberg.org/mebyus/goku/lib/ku/ir/cer"
	"codeberg.org/mebyus/goku/lib/ku/ir/symbol"
	"codeberg.org/mebyus/goku/lib/ku/ir/typ"
	"codeberg.org/mebyus/goku/lib/ku/token"
)

// SourcePile builds a Unit from its source files
type SourcePile struct {
	unit *Unit

	tdefs  []ast.TypeDefinition
	fndecs []ast.FunctionDeclaration
	fndefs []ast.FunctionDefinition
	cdefs  []ast.TopLevelConst
	vdefs  []ast.TopLevelVar

	// maintains unit-wide list of top-level symbols with no definition
	//
	// maps symbol name to index in function declarations slice
	undefined map[string]int
}

func NewSourcePile(name string, hash uint64) *SourcePile {
	return &SourcePile{
		unit: NewUnit(name, hash),

		undefined: make(map[string]int),
	}
}

func ConstructUnit(name string, hash uint64, srcs []*ast.UnitSourceCode, imps []Import) (*Unit, error) {
	p := NewSourcePile(name, hash)

	for _, imp := range imps {
		name := imp.Name
		sym := p.unit.Scope.Sym(name)
		if sym != nil {
			return nil, &cer.Error{
				Span: imp.Pos,
				Msg:  name,
				Kind: cer.MultipleSymbolDefinitions,
			}
		}
		sym = &Symbol{
			Name: name,
			Def:  imp.Unit,
			Pos:  imp.Pos,
			Kind: symbol.Import,
			With: typ.With{Typ: &typ.Type{
				// TODO: fill out Base
				Def:  imp.Unit,
				Kind: typ.Unit,
			}},
		}

		p.unit.byName[name] = len(p.unit.Symbols)
		p.unit.Symbols = append(p.unit.Symbols, sym)
		p.unit.Scope.Add(sym)
	}

	for _, src := range srcs {
		err := p.Add(src)
		if err != nil {
			return nil, err
		}
	}

	return p.construct()
}

func (p *SourcePile) Add(src *ast.UnitSourceCode) error {
	for _, typ := range src.Types {
		name := typ.Name.Lit
		sym := p.unit.Scope.Sym(name)
		if sym != nil {
			return &cer.Error{
				Span: typ.Name.Pos,
				Msg:  name,
				Kind: cer.MultipleSymbolDefinitions,
			}
		}

		sym = &Symbol{
			Pos:    typ.Name.Pos,
			Kind:   symbol.Type,
			Public: typ.Public,
			Name:   name,
		}
		if typ.Public {
			sym.Unit = p.unit
		}

		p.unit.byName[name] = len(p.unit.Symbols)
		p.unit.Symbols = append(p.unit.Symbols, sym)
		p.unit.Scope.Add(sym)

		p.tdefs = append(p.tdefs, typ)
	}

	for _, decl := range src.FunctionDeclarations {
		name := decl.Name.Lit
		_, ok := p.undefined[name]
		if ok {
			return &cer.Error{
				Span: decl.Name.Pos,
				Msg:  name,
				Kind: cer.MultipleSymbolDeclarations,
			}
		}
		p.undefined[name] = len(p.fndecs)

		// TODO: we need a way to create symbols for functions with
		// definitions which are going to be external to ku code

		p.unit.byName[name] = len(p.unit.Symbols)
		p.fndecs = append(p.fndecs, decl)
	}

	for _, fn := range src.FunctionDefinitions {
		name := fn.Declaration.Name.Lit
		sym := p.unit.Scope.Sym(name)
		if sym != nil {
			return &cer.Error{
				Span: fn.Declaration.Name.Pos,
				Msg:  name,
				Kind: cer.MultipleSymbolDefinitions,
			}
		}

		_, ok := p.undefined[name]
		if ok {
			// TODO: check that function signatures from declaration and definition match each other
			delete(p.undefined, name)
		}

		var fnArgs []*Arg
		var sig typ.Signature
		parameters := fn.Declaration.Signature.Parameters
		if parameters != nil {
			args := parameters.(ast.NamedTuple)
			for _, arg := range args {
				name := arg.Name.Lit

				t, err := p.unit.Scope.Spec(arg.Type)
				if err != nil {
					return err
				}

				sym = &Symbol{
					Pos:  arg.Name.Pos,
					With: typ.With{Typ: t},
					Name: name,
					Kind: symbol.Arg,
				}
				fnArg := &Arg{
					Name:   name,
					Symbol: sym,
					Type:   t,
				}
				sym.Def = fnArg
				fnArgs = append(fnArgs, fnArg)
				sig.Args = append(sig.Args, t)
			}
		}

		resultType, err := p.unit.Scope.Spec(fn.Declaration.Signature.Result)
		if err != nil {
			return err
		}

		sig.Result = resultType
		sym = &Symbol{
			Pos:    fn.Declaration.Name.Pos,
			Kind:   symbol.Fn,
			Public: fn.Declaration.Public,
			Name:   name,
			With: typ.With{
				// TODO: somehow move this to tindex
				Typ: &typ.Type{
					Kind: typ.Function,
					Def:  sig,
				},
			},
			Def: &Fn{
				Name: name,
				Type: resultType,
				Args: fnArgs,
			},
		}
		if fn.Declaration.Public {
			sym.Unit = p.unit
		}

		p.unit.byName[name] = len(p.unit.Symbols)
		p.unit.Symbols = append(p.unit.Symbols, sym)
		p.unit.Scope.Add(sym)

		p.fndefs = append(p.fndefs, fn)
	}

	for _, c := range src.Constants {
		short := c.Const.(ast.ShortAssign)

		name := short.Name.Lit
		sym := &Symbol{
			Pos:    short.Name.Pos,
			Kind:   symbol.Const,
			Public: c.Public,
			Name:   name,
		}

		p.unit.byName[name] = len(p.unit.Symbols)
		p.unit.Symbols = append(p.unit.Symbols, sym)
		p.unit.Scope.Add(sym)

		p.cdefs = append(p.cdefs, c)
	}

	for _, v := range src.Variables {
		def := v.Var.(ast.VarDefinition)

		t, err := p.unit.Scope.Spec(def.Declaration.Type)
		if err != nil {
			return err
		}

		name := def.Declaration.Name.Lit
		sym := &Symbol{
			Pos:    def.Declaration.Name.Pos,
			Kind:   symbol.Var,
			Public: v.Public,
			Name:   name,
			With:   typ.With{Typ: t},
		}

		p.unit.byName[name] = len(p.unit.Symbols)
		p.unit.Symbols = append(p.unit.Symbols, sym)
		p.unit.Scope.Add(sym)

		p.vdefs = append(p.vdefs, v)
	}

	return nil
}

func (p *SourcePile) construct() (*Unit, error) {
	for name, idx := range p.undefined {
		fmt.Printf("warn: no definition found for symbol %s: %s\n", p.fndecs[idx].Name.Pos.String(), name)
	}

	for _, t := range p.tdefs {
		name := t.Name.Lit
		sym := p.unit.Scope.Sym(name)
		if sym == nil {
			panic("missing type sym " + name)
		}

		def, err := p.unit.Scope.Spec(t.Spec)
		if err != nil {
			return nil, err
		}

		sym.Def = &typ.Type{
			Name: name,
			Kind: typ.Named,
			Base: def.Base,
		}
	}

	for _, fn := range p.fndefs {
		name := fn.Declaration.Name.Lit
		sym := p.unit.Scope.Sym(name)
		if sym == nil {
			panic("missing fn sym " + name)
		}

		def, err := FnDef(p.unit.Scope, fn)
		if err != nil {
			return nil, err
		}

		fn := sym.Def.(*Fn)
		fn.Args = def.Args
		fn.Body = def.Body
		fn.Refs = def.Refs
	}

	for _, fn := range p.fndecs {
		name := fn.Name.Lit

		_, ok := p.undefined[name]
		if !ok {
			continue
		}

		sym := p.unit.Scope.Sym(name)
		if sym != nil {
			panic("fn " + name + " declaration sym of unknown origin")
		}

		var fnArgs []*Arg
		var sig typ.Signature
		parameters := fn.Signature.Parameters
		if parameters != nil {
			args := parameters.(ast.NamedTuple)
			for _, arg := range args {
				name := arg.Name.Lit

				t, err := p.unit.Scope.Spec(arg.Type)
				if err != nil {
					return nil, err
				}

				sym = &Symbol{
					Pos:  arg.Name.Pos,
					With: typ.With{Typ: t},
					Name: name,
					Kind: symbol.Arg,
				}
				fnArg := &Arg{
					Name:   name,
					Symbol: sym,
					Type:   t,
				}
				sym.Def = fnArg
				fnArgs = append(fnArgs, fnArg)
				sig.Args = append(sig.Args, t)
			}
		}

		var resultType *typ.Type
		var isNeverReturn bool

		neverToken, ok := fn.Signature.Result.(ast.Never)
		if ok && neverToken.Kind == token.Never {
			isNeverReturn = true
		} else {
			var err error
			resultType, err = p.unit.Scope.Spec(fn.Signature.Result)
			if err != nil {
				return nil, err
			}
		}

		sig.Result = resultType
		sig.Never = isNeverReturn

		sym = &Symbol{
			Pos:    fn.Name.Pos,
			Kind:   symbol.Fn,
			Public: fn.Public,
			Name:   name,
			With: typ.With{
				// TODO: somehow move this to tindex
				Typ: &typ.Type{
					Kind: typ.Function,
					Def:  sig,
				},
			},
			Def: &Fn{
				Name: name,
				Type: resultType,
				Args: fnArgs,
			},
		}
		if fn.Public {
			sym.Unit = p.unit
		}

		p.unit.byName[name] = len(p.unit.Symbols)
		p.unit.Symbols = append(p.unit.Symbols, sym)
		p.unit.Scope.Add(sym)
	}

	err := p.unit.hoist()
	if err != nil {
		return nil, err
	}
	return p.unit, nil
}
