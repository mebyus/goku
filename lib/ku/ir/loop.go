package ir

type ForEver struct {
	Body Block
}

type While struct {
	Body Block

	Condition any
}
