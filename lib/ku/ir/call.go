package ir

import (
	"codeberg.org/mebyus/goku/lib/ku/ir/typ"
	"codeberg.org/mebyus/goku/lib/ku/source"
)

type Call struct {
	// call result type
	typ.With

	// expression being called, must be of function type
	Target typ.Typed

	// list of expressions - call arguments
	Args []typ.Typed
}

// Select represents expression of the form <expr>.target
type Select struct {
	Pos source.Pos

	typ.With

	// Name of selected field/symbol
	Name string

	// Result of select operation. Meaning heavily depends on selectee type:
	//
	//	- Import: pointer to Symbol struct (of imported symbol)
	//	- Struct: typ.Field
	Result any

	// Expression being selected from
	Target typ.Typed
}
