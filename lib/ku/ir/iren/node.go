// package iren (Intermediate Representation Expression Node) contains abstractions
// which represent expression nodes in intermediate representation
//
// Mainly exist to separate namespace from other packages and prevent name cluttering
// between similar concepts (e.g. intermediate representation Node and expression Node)
package iren

import "codeberg.org/mebyus/goku/lib/ku/ir/typ"

type Node struct {
	typ.Typed
}
