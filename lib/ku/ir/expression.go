package ir

import (
	"fmt"
	"strconv"

	"codeberg.org/mebyus/goku/lib/ku/ast"
	"codeberg.org/mebyus/goku/lib/ku/ast/oper"
	"codeberg.org/mebyus/goku/lib/ku/ir/cer"
	"codeberg.org/mebyus/goku/lib/ku/ir/scope"
	"codeberg.org/mebyus/goku/lib/ku/ir/symbol"
	"codeberg.org/mebyus/goku/lib/ku/ir/typ"
	"codeberg.org/mebyus/goku/lib/ku/token"
)

type Op uint8

// Execute statement executes expression without assigning its result
type Execute struct {
	Expr any
}

const (
	Nop Op = iota
	Add
	Sub
	Plus
	Minus
	Mult
	Div
	Mod

	BitAnd
	BitOr
	Xor
	LeftShift
	RightShift

	Not
	And
	Or

	Equal
	NotEqual
	Less
	Greater
	LessEqual
	GreaterEqual
)

var textOp = [...]string{
	Nop:   "<nop>",
	Add:   "+",
	Sub:   "-",
	Plus:  "+",
	Minus: "-",
	Mult:  "*",
	Div:   "/",
	Mod:   "%",

	BitAnd:     "&",
	BitOr:      "|",
	Xor:        "^",
	LeftShift:  "<<",
	RightShift: ">>",

	Not: "!",
	And: "&&",
	Or:  "||",

	Equal:        "==",
	NotEqual:     "!=",
	Less:         "<",
	Greater:      ">",
	LessEqual:    "<=",
	GreaterEqual: ">=",
}

func (op Op) String() string {
	return textOp[op]
}

type Integer struct {
	// for values which cannot fit in 64 bits
	Lit string

	Val uint64
}

func (Integer) Type() *typ.Type {
	return typ.I
}

func (Integer) Purity() typ.Purity {
	return typ.Static
}

type Float struct {
	Lit string
}

func (Float) Type() *typ.Type {
	return typ.F
}

type Char struct {
	Val uint64
}

type Str struct {
	Lit string
}

func (Str) Type() *typ.Type {
	return typ.S
}

func (Str) Purity() typ.Purity {
	return typ.Static
}

type Nil struct{}

type True struct{}

func (True) Type() *typ.Type {
	return typ.B
}

func (True) Purity() typ.Purity {
	return typ.Static
}

type False struct{}

func (False) Type() *typ.Type {
	return typ.B
}

func (False) Purity() typ.Purity {
	return typ.Static
}

// usage of identifier as value (not function or method call, selector or index)
type Usage struct {
	typ.With

	Name string

	Symbol *Symbol
}

type Index struct {
	typ.With

	Target any // can be Usage, Index, Selector, Call
	Index  any // expression
}

type List struct {
	typ.With

	Elems []typ.Typed
}

// expression inside parenthesis
type Pares struct {
	typ.With

	Inner typ.Typed
}

func (s *Scope) pares(expr ast.ParenthesizedExpression, refs map[*Symbol]struct{}) (*Pares, error) {
	inner, err := s.expr(expr.Inner, refs)
	if err != nil {
		return nil, err
	}
	return &Pares{
		With:  typ.Derive(inner),
		Inner: inner,
	}, nil
}

// type Literal struct {
// 	Derived

// 	L any
// }

type Unary struct {
	typ.With

	Operand any

	Op Op
}

type Binary struct {
	typ.With

	Left  any
	Right any

	Op Op
}

func unOp(op oper.Unary) Op {
	switch op.Kind {
	case token.Plus:
		return Plus
	case token.Minus:
		return Minus
	default:
		panic(op)
	}
}

func binOp(op oper.Binary) Op {
	switch op.Kind {
	case token.Asterisk:
		return Mult
	case token.Slash:
		return Div
	case token.Percent:
		return Mod
	case token.Plus:
		return Add
	case token.Minus:
		return Sub
	case token.Equal:
		return Equal
	case token.NotEqual:
		return NotEqual
	case token.Less:
		return Less
	case token.Greater:
		return Greater
	case token.LessOrEqual:
		return LessEqual
	case token.GreaterOrEqual:
		return GreaterEqual
	case token.LogicalAnd:
		return And
	case token.LogicalOr:
		return Or
	case token.LeftShift:
		return LeftShift
	case token.RightShift:
		return RightShift
	case token.Caret:
		return Xor
	case token.Ampersand:
		return BitAnd
	case token.Pipe:
		return BitOr
	default:
		panic("unexpected token kind " + strconv.FormatInt(int64(op.Kind), 10))
	}
}

func (s *Scope) expr(expr ast.Expression, refs map[*Symbol]struct{}) (typ.Typed, error) {
	switch x := expr.(type) {
	case ast.BinaryExpression:
		return s.binary(x, refs)
	case *ast.UnaryExpression:
		return s.unary(x, refs)
	case ast.Operand:
		return s.operand(x, refs)
	default:
		panic(x)
	}
}

func basic(lit ast.BasicLiteral) (typ.Typed, error) {
	k := lit.Kind

	switch k {
	case token.DecimalInteger:
		return Integer{
			Lit: lit.Lit,
			Val: lit.Val,
		}, nil
	case token.DecimalFloat:
		return Float{
			Lit: lit.Lit,
		}, nil
	case token.True:
		return True{}, nil
	case token.False:
		return False{}, nil
	default:
		panic(k)
	}
}

func (s *Scope) operand(expr ast.Operand, refs map[*Symbol]struct{}) (typ.Typed, error) {
	switch x := expr.(type) {
	case ast.BasicLiteral:
		return basic(x)
	case ast.Identifier:
		name := x.Lit
		sym := s.Find(name, x.Pos.Ofs)
		if sym == nil {
			return nil, &cer.Error{
				Kind: cer.UnresolvedSymbol,
				Span: x.Pos,
				Msg:  name,
			}
		}
		if sym.Scope.Kind == scope.Unit {
			refs[sym] = struct{}{}
		}
		return Usage{
			With:   typ.Derive(sym),
			Name:   name,
			Symbol: sym,
		}, nil
	case ast.ParenthesizedExpression:
		return s.pares(x, refs)
	case ast.CallExpression:
		ident, ok := x.Callee.(ast.Identifier)
		if ok {
			name := ident.Lit
			sym := s.Find(name, ident.Pos.Ofs)
			if sym == nil {
				return nil, &cer.Error{
					Kind: cer.UnresolvedSymbol,
					Span: ident.Pos,
					Msg:  name,
				}
			}
			if sym.Kind != symbol.Fn {
				return nil, &cer.Error{
					Kind: cer.NotCallableSymbol,
					Span: ident.Pos,
					Msg:  name,
				}
			}
			if sym.Scope.Kind == scope.Unit {
				refs[sym] = struct{}{}
			}
			fn := sym.Def.(*Fn)
			if len(x.Arguments.List) != len(fn.Args) {
				got := len(x.Arguments.List)
				want := len(fn.Args)
				if got > want {
					return nil, &cer.Error{
						Kind: cer.TooManyArguments,
						Span: ident.Pos,
						Msg:  fmt.Sprintf("expected %d, got %d", want, got),
					}
				}
				return nil, &cer.Error{
					Kind: cer.NotEnoughArguments,
					Span: ident.Pos,
					Msg:  fmt.Sprintf("expected %d, got %d", want, got),
				}
			}
			var args []typ.Typed
			if len(x.Arguments.List) != 0 {
				args = make([]typ.Typed, 0, len(x.Arguments.List))
				for i, arg := range x.Arguments.List {
					a, err := s.expr(arg, refs)
					if err != nil {
						return nil, err
					}
					_, err = deduceArgType(a.Type(), fn.Args[i].Type)
					if err != nil {
						return nil, err
					}
					args = append(args, a)
				}
			}
			return Call{
				With:   typ.With{Typ: fn.Type},
				Target: sym,
				Args:   args,
			}, nil
		}

		sel, ok := x.Callee.(ast.SelectorExpression)
		if !ok {
			panic(fmt.Sprintf("not implemented for %T", x.Callee))
		}

		target, ok := sel.Target.(ast.Identifier)
		if !ok {
			panic(fmt.Sprintf("not implemented for %T", sel.Target))
		}

		name := target.Lit
		sym := s.Find(name, target.Pos.Ofs)
		if sym == nil {
			return nil, &cer.Error{
				Kind: cer.UnresolvedSymbol,
				Span: target.Pos,
				Msg:  name,
			}
		}

		if sym.Kind != symbol.Import {
			panic(fmt.Sprintf("not implemented for %s", sym.Kind.String()))
		}

		unit := sym.Def.(*Unit)
		field := sel.Selected.Lit
		refSym, err := unit.Exp(field)
		if err != nil {
			err.(*cer.Error).Span = sel.Selected.Pos
			return nil, err
		}

		if refSym.Kind != symbol.Fn {
			panic(fmt.Sprintf("not implemented for %s", refSym.Kind.String()))
		}

		sig := refSym.Type().Def.(typ.Signature)

		if len(x.Arguments.List) != len(sig.Args) {
			got := len(x.Arguments.List)
			want := len(sig.Args)
			if got > want {
				return nil, &cer.Error{
					Kind: cer.TooManyArguments,
					Span: sel.Selected.Pos,
					Msg:  fmt.Sprintf("expected %d, got %d", want, got),
				}
			}
			return nil, &cer.Error{
				Kind: cer.NotEnoughArguments,
				Span: sel.Selected.Pos,
				Msg:  fmt.Sprintf("expected %d, got %d", want, got),
			}
		}
		var args []typ.Typed
		if len(x.Arguments.List) != 0 {
			args = make([]typ.Typed, 0, len(x.Arguments.List))
			for i, arg := range x.Arguments.List {
				a, err := s.expr(arg, refs)
				if err != nil {
					return nil, err
				}
				_, err = deduceArgType(a.Type(), sig.Args[i])
				if err != nil {
					return nil, err
				}
				args = append(args, a)
			}
		}

		return Call{
			With: typ.With{Typ: sig.Result},
			Args: args,
			Target: Select{
				With:   typ.Derive(refSym),
				Pos:    sel.Selected.Pos,
				Name:   field,
				Result: refSym,

				Target: Usage{
					With:   typ.Derive(sym),
					Name:   name,
					Symbol: sym,
				},
			},
		}, nil
	case ast.List:
		panic(x)
	default:
		panic(x)
	}
}

func deduceArgType(got, want *typ.Type) (*typ.Type, error) {
	if got == want {
		return want, nil
	}
	switch want.Kind {
	case typ.Signed, typ.Unsigned:
		if got.Kind == typ.StaticInteger {
			return want, nil
		}
		return nil, fmt.Errorf("expected argument of type %s, but got %s", want.Name, got.Name)
	default:
		panic(fmt.Sprintf("not implemented for %s", want.Kind.String()))
	}
}

func (s *Scope) unary(expr *ast.UnaryExpression, refs map[*Symbol]struct{}) (Unary, error) {
	op := unOp(expr.Operator)

	switch x := expr.UnaryOperand.(type) {
	case *ast.UnaryExpression:
		u, err := s.unary(x, refs)
		if err != nil {
			return Unary{}, nil
		}
		return Unary{
			With:    typ.Derive(u),
			Operand: u,
			Op:      op,
		}, nil
	case ast.Operand:
		o, err := s.operand(x, refs)
		if err != nil {
			return Unary{}, err
		}
		return Unary{
			With:    typ.Derive(o),
			Operand: o,
			Op:      op,
		}, nil
	default:
		panic(x)
	}
}

func (s *Scope) binary(expr ast.BinaryExpression, refs map[*Symbol]struct{}) (Binary, error) {
	left, err := s.expr(expr.LeftSide, refs)
	if err != nil {
		return Binary{}, err
	}
	right, err := s.expr(expr.RightSide, refs)
	if err != nil {
		return Binary{}, err
	}
	op := binOp(expr.Operator)
	lt := left.Type()
	rt := right.Type()
	t, err := deduceBinaryType(op, lt, rt)
	if err != nil {
		return Binary{}, err
	}
	return Binary{
		With:  typ.With{Typ: t},
		Left:  left,
		Right: right,
		Op:    op,
	}, nil
}

// evalBinaryExpr checks compatibility of operands in binary expression and returns
// resulting expression node if everything is correct. If both operands are static,
// this function also simplifies (evaluates) the result of expression
func evalBinaryExpr(op Op, left, right typ.Typed) (any, error) {
	lt := left.Type()
	rt := right.Type()
	if lt.IsStatic() && rt.IsStatic() {
		// if both nodes are static result can be obtained right here
		switch lt.Kind {
		case typ.StaticInteger:
		case typ.StaticFloat:
		case typ.StaticBoolean:
		case typ.StaticString:
		case typ.StaticNil:
			return nil, &cer.Error{
				// Pos: left.,
			}
		default:
			panic(fmt.Sprintf("unexpected kind %s, switch must cover all cases", lt.Kind.String()))
		}
	}
	return Binary{}, nil
}

func deduceBinaryType(op Op, left, right *typ.Type) (*typ.Type, error) {
	if left == right {
		switch op {
		case Add, Sub, Mult, Div:
			return left, nil
		case Equal, NotEqual:
			return typ.Bool, nil
		case Or:
			return typ.Bool, nil
		default:
			panic(fmt.Sprintf("not implemented for %d", op))
		}
	}
	switch op {
	case Add, Sub, Mult, Div:
		if left.Kind == typ.StaticInteger {
			switch right.Kind {
			case typ.Signed, typ.Unsigned:
				return right, nil
			default:
				panic(fmt.Sprintf("not implemented for %s", right.Kind.String()))
			}
		}

		if right.Kind == typ.StaticInteger {
			switch left.Kind {
			case typ.Signed, typ.Unsigned:
				return left, nil
			default:
				panic(fmt.Sprintf("not implemented for %s", left.Kind.String()))
			}
		}
		panic(fmt.Sprintf("not implemented for %s and %s", left.Kind.String(), right.Kind.String()))
	case Equal, NotEqual, Less, LessEqual, Greater, GreaterEqual:
		if left.Kind == typ.StaticInteger {
			switch right.Kind {
			case typ.Signed, typ.Unsigned:
				return typ.Bool, nil
			default:
				panic(fmt.Sprintf("not implemented for %s", right.Kind.String()))
			}
		}

		if right.Kind == typ.StaticInteger {
			switch left.Kind {
			case typ.Signed, typ.Unsigned:
				return typ.Bool, nil
			default:
				panic(fmt.Sprintf("not implemented for %s", left.Kind.String()))
			}
		}
		panic(fmt.Sprintf("not implemented for %s and %s", left.Kind.String(), right.Kind.String()))
	case Or:
		if left.Kind == typ.StaticInteger {
			switch right.Kind {
			case typ.Signed, typ.Unsigned:
				return typ.Bool, nil
			default:
				panic(fmt.Sprintf("not implemented for %s", right.Kind.String()))
			}
		}

		if right.Kind == typ.StaticInteger {
			switch left.Kind {
			case typ.Signed, typ.Unsigned:
				return typ.Bool, nil
			default:
				panic(fmt.Sprintf("not implemented for %s", left.Kind.String()))
			}
		}
		panic(fmt.Sprintf("not implemented for %s and %s", left.Kind.String(), right.Kind.String()))
	default:
		panic(fmt.Sprintf("not implemented for %d", op))
	}
}
