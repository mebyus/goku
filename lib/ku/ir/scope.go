package ir

import "codeberg.org/mebyus/goku/lib/ku/ir/scope"

type Scope struct {
	// maps name to its local Symbol
	Names map[string]*Symbol

	Parent *Scope

	// Types which are visible inside this Scope
	Types *Tindex

	// local position in Parent Scope
	Pos uint32

	Kind scope.Kind
}

func (s *Scope) Find(name string, pos uint32) *Symbol {
	sym, ok := s.Names[name]
	if !ok || (s.Kind > scope.Unit && pos < sym.Pos.Ofs) {
		if s.Parent == nil {
			return nil
		}
		return s.Parent.Find(name, s.Pos)
	}
	return sym
}

func (s *Scope) Sym(name string) *Symbol {
	return s.Names[name]
}

func (s *Scope) Add(sym *Symbol) {
	sym.Scope = s
	s.Names[sym.Name] = sym
}
