package ir

import (
	"fmt"

	"codeberg.org/mebyus/goku/lib/ku/hoister"
	"codeberg.org/mebyus/goku/lib/ku/ir/cer"
	"codeberg.org/mebyus/goku/lib/ku/ir/scope"
	"codeberg.org/mebyus/goku/lib/ku/ir/symbol"
	"codeberg.org/mebyus/goku/lib/ku/ir/typ"
)

type Unit struct {
	Symbols []*Symbol

	Graph *hoister.Graph[*Symbol]

	Name string

	// Unit level Scope
	Scope *Scope

	// Contains types which are defined in this Unit
	Types *Tindex

	// Type of symbol when this unit is imported
	Type *typ.Type

	// Maps symbol name to its index in Symbols slice
	byName map[string]int

	// unit origin.Path hash
	hash uint64
}

func NewUnit(name string, hash uint64) *Unit {
	tx := NewTindex()
	t := &typ.Type{
		Kind: typ.Unit,
	}
	t.Base = t
	u := &Unit{
		Name: name,
		Scope: &Scope{
			Parent: global,
			Types:  tx,
			Names:  make(map[string]*Symbol),
			Kind:   scope.Unit,
		},
		Types:  tx,
		Type:   t,
		byName: make(map[string]int),
		hash:   hash,
	}
	t.Def = u
	return u
}

// Exp gives exported top-level unit symbol by its name. If symbol is not found
// or unexported an error is returned
func (u *Unit) Exp(name string) (*Symbol, error) {
	idx, ok := u.byName[name]
	if ok {
		sym := u.Symbols[idx]
		if sym.Public {
			return sym, nil
		}
		return nil, &cer.Error{
			Kind: cer.NotExportedSymbol,
			Msg:  fmt.Sprintf("symbol \"%s\" is not exported in unit \"%s\"", name, u.Name),
		}
	}
	return nil, &cer.Error{
		Kind: cer.NotFoundInUnit,
		Msg:  fmt.Sprintf("unit \"%s\" does not contain symbol \"%s\"", u.Name, name),
	}
}

// construct unit-level symbol dependency graph
func (u *Unit) hoist() error {
	if len(u.Symbols) == 0 {
		return &cer.Error{
			Kind: cer.EmptyUnit,
			Msg:  u.Name,
		}
	}

	var err error
	scribe := hoister.New[*Symbol]()
	for _, sym := range u.Symbols {
		sym.Unit = u

		switch sym.Kind {
		case symbol.Const:
			// TODO: add const and var ancestors
			err = scribe.Alpha(sym, nil)
		case symbol.Var:
			err = scribe.Alpha(sym, nil)
		case symbol.Type:
			err = scribe.Alpha(sym, nil)
		case symbol.Fn:
			err = scribe.Beta(sym, sym.Def.(*Fn).Refs)
		case symbol.Import:
			// do nothing since imports are automatically hoisted
			// by requiring no import cycles
		default:
			panic(fmt.Sprintf("not implemented for %s", sym.Kind.String()))
		}

		if err != nil {
			return err
		}
	}

	u.Graph, err = scribe.Graph()
	return err
}
