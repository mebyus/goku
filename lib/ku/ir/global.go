package ir

import (
	"codeberg.org/mebyus/goku/lib/ku/ir/scope"
	"codeberg.org/mebyus/goku/lib/ku/ir/symbol"
	"codeberg.org/mebyus/goku/lib/ku/ir/typ"
)

var u8 = &Symbol{
	Name: "u8",
	Def:  typ.U8,
	Kind: symbol.Type,
}

var u16 = &Symbol{
	Name: "u16",
	Kind: symbol.Type,
}

var u32 = &Symbol{
	Name: "u32",
	Def:  typ.U32,
	Kind: symbol.Type,
}

var u64 = &Symbol{
	Name: "u64",
	Def:  typ.U64,
	Kind: symbol.Type,
}

var usz = &Symbol{
	Name: "usz",
	Def:  typ.USZ,
	Kind: symbol.Type,
}

var i8 = &Symbol{
	Name: "i8",
	Kind: symbol.Type,
}

var i16 = &Symbol{
	Name: "i16",
	Kind: symbol.Type,
}

var i32 = &Symbol{
	Name: "i32",
	Def:  typ.I32,
	Kind: symbol.Type,
}

var i64 = &Symbol{
	Name: "i64",
	Def:  typ.I64,
	Kind: symbol.Type,
}

var isz = &Symbol{
	Name: "isz",
	Def:  typ.ISZ,
	Kind: symbol.Type,
}

var symBool = &Symbol{
	Name: "bool",
	Def:  typ.Bool,
	Kind: symbol.Type,
}

var str = &Symbol{
	Name: "str",
	Def:  typ.Str,
	Kind: symbol.Type,
}

var global = &Scope{
	Kind: scope.Global,

	Names: map[string]*Symbol{
		u8.Name:  u8,
		u16.Name: u16,
		u32.Name: u32,
		u64.Name: u64,

		usz.Name: usz,

		i8.Name:  i8,
		i16.Name: i16,
		i32.Name: i32,
		i64.Name: i64,

		isz.Name: isz,

		symBool.Name: symBool,

		str.Name: str,
	},
}

func init() {
	u8.Scope = global
	u16.Scope = global
	u32.Scope = global
	u64.Scope = global

	usz.Scope = global

	i8.Scope = global
	i16.Scope = global
	i32.Scope = global
	i64.Scope = global

	isz.Scope = global

	symBool.Scope = global

	str.Scope = global
}
