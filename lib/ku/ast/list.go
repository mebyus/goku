package ast

// <List> = ".[" [ <Expression> ] { "," <Expression> } [ "," ] "]"
//
// List is a literal for arrau or chunk types
type List struct {
	Elems []Expression
}
