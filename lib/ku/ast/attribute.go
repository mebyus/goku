package ast

import "codeberg.org/mebyus/goku/lib/ku/source"

// AtrBlock block with list of attributes. Attributes can modify symbol
// compile and runtime behaviour
type AtrBlock struct {
	// starting position of block
	Pos source.Pos

	List []any
}
