package ast

import "codeberg.org/mebyus/goku/lib/ku/source"

type Node interface {
	source.Span
}
