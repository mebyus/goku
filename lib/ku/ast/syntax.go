package ast

// Language syntax in Backus-Naur extended form
//
// <SourceText> = <ModuleClause> [ <ImportClause> ] { <TopLevelDeclaration> } <EOF>
//
// <ModuleClause> = "module" <ModuleName>
//
// <ModuleName> = <Identifier>
//
// <TopLevelDeclaration> = <ConstDeclaration> | <TypeDeclaration> | <VarDeclaration> | <FunctionDeclaration> |
//                         <FunctionDefinition>
//
// <FunctionDeclaration> = "fn" <FunctionName> <FunctionSignature>
//
// <FunctionDefinition> = <FunctionDeclaration> <FunctionBody>
//
// <FunctionSignature> = <FunctionParameters> [ "=>" <FunctionResult> ]
//
// <FunctionResult> = <FunctionParameters> | <TypeSpecifier>
//
// <FunctionParameters> = "(" [ <ParameterList> [ "," ] ] ")"
//
// <ParameterList>  = <ParameterDeclaration> { "," <ParameterDeclaration> }
//
// <ParameterDeclaration>  = [ <IdentifierList> ":" ] [ "..." ] <TypeSpecifier>
//
// <IdentifierList> = <Identifier> { "," <Identifier> }
//
// <FunctionBody> = <BlockStatement>
//
// <FunctionName> = <Identifier>
//
// <TypeSpecifier> = <TypeName> | <TypeLiteral>
//
// <TypeName> = <Identifier> | <QualifiedIdentifier>
//
// <QualifiedIdentifier> = <ModuleName> "." <Identifier>
//
// <TypeLiteral> = <ArrayTypeLiteral> | <PointerTypeLiteral> | <SliceTypeLiteral> | <MapTypeLiteral> |
//                 <ChannelTypeLiteral> | <SetTypeLiteral>
//
// <ArrayTypeLiteral> = "[" <ArrayLengthSpecifier> "]" <ElementTypeSpecifier>
//
// <ArrayLengthSpecifier> = <Expression>
//
// <ElementTypeSpecifier> = <TypeSpecifier>
//
// <PointerTypeLiteral> = "*" <BaseTypeSpecifier>
//
// <BaseTypeSpecifier> = <TypeSpecifier>
//
// <SliceTypeLiteral> = "[" "]" <ElementTypeSpecifier>
//
// <MapTypeLiteral> = "[" <KeyTypeSpecifier> "]" "=>" <ValueTypeSpecifier>
//
// <KeyTypeSpecifier> = <TypeSpecifier>
//
// <ValueTypeSpecifier> = <TypeSpecifier>
//
// <SetTypeLiteral> = "[" <ElementTypeSpecifier> "]"
//
// <ConstDeclaration> = "const", <ConstSpec>
//
// <ConstSpec> = <SingleLineConstSpec> | <MultiLineConstSpec>
//
// <SingleLineConstSpec> = <IdentifierList>, [ <Type> ], "=", <ExpressionList>, <TERM>
//
// <MultiLineConstSpec> = <SingleLineConstSpec>, { <SingleLineConstSpec> }
//
// <BlockStatement> = "{", <StatementList>, "}"
//
// <StatementList> = { <Statement> };
//
// <Statement> = <BlockStatement> | <AssignStatement> | <DefineStatement> | <IfStatement> |
//               <DeferStatement> | <ExpressionStatement> | <ReturnStatement> | <MatchStatement> |
//               <LoopStatement>
//
// <DeferStatement> = "defer", <DeferClause>;
//
// <IfStatement> = <IfClause>, { <ElifClause> }, [ <ElseClause> ];
//
// <IfClause> = "if", <Expression>, <BlockStatement>;
//
// <ElifClause> = "elif", <Expression>, <BlockStatement>;
//
// <ElseClause> = "else", <BlockStatement>;
//
// <ExpressionStatement> = <Expression>, ";";
//
// <DeferClause> = <CallExpression>, <TERM> | <BlockStatement>;
//
// <DefineStatement> = [ "imt" ], <Identifier>, { ",", <Identifier> }, ":=", <Expression>, { ",", <Expression> }, ";";
//
// <SelectorExpression> = <Expression>, ".", <Identifier>
//
// <CallExpression> = <Expression>, "(", { <Expression>, "," }, ")";
//
// <DestinationExpression> = <IndexExpression> | <Identifier>
//
// <AssignStatement> = <DestinationExpression>, { ",", <DestinationExpression> }, "=", <Expression>, { ",",
//                     <Expression> }, ";";
//
// <ReturnStatement> = "return", [ <Expression> ], ";";
