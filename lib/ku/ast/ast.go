package ast

import (
	"codeberg.org/mebyus/goku/lib/ku/source"
	"codeberg.org/mebyus/goku/lib/ku/token"
)

// <SourceCode> = <UnitSourceCode> | <ScriptSourceCode>
type SourceCode interface{}

// <UnitSourceCode> = <UnitClause> [ <ImportClause> ] { <TopLevelDeclaration> } <EOF>
type UnitSourceCode struct {
	// Name of the file that produced this unit part
	File *source.File

	Unit UnitClause
	TopLevelDeclarations
}

// <ScriptSourceCode> = [ <ImportClause> ] { <TopLevelDeclaration> | <Statement> } <EOF>
type ScriptSourceCode struct {
	TopLevelDeclarations
	Statements []Statement
}

// <TopLevelDeclaration> = <ConstDeclaration> | <TypeDeclaration> | <VarDeclaration> | <FunctionDeclaration> |
//
//	<FunctionDefinition>
type TopLevelDeclarations struct {
	Imports              []ImportBlock
	Constants            []TopLevelConst
	Types                []TypeDefinition
	Variables            []TopLevelVar
	FunctionDeclarations []FunctionDeclaration
	FunctionDefinitions  []FunctionDefinition
}

// <TypeDefinition> = "type" <Name> <TypeSpecifier>
type TypeDefinition struct {
	Name Identifier
	Spec TypeSpecifier

	Public bool
}

// <UnitClause> = "unit" <UnitName>
type UnitClause struct {
	Name Identifier
}

// <TypeSpecifier> = <TypeName> | <TypeLiteral>
type TypeSpecifier any

// <StructType> = "struct" "{" { <FieldDefinition> "," } "}"
type StructType struct {
	Fields []FieldDefinition
}

// <TypeLiteral> = <ArrayTypeLiteral> | <PointerTypeLiteral> | <SliceTypeLiteral> | <MapTypeLiteral> |
//
//	<ChannelTypeLiteral> | <SetTypeLiteral> | <Tuple>
type TypeLiteral interface{}

// <Never> = "never"
type Never token.Token

// <TypeName> = <Identifier> | <QualifiedIdentifier>
type TypeName interface{}

// <SingularExpression> = <PrimaryExpression> | <UnaryExpression>
type SingularExpression interface{}

// BasicOperand     = Literal | OperandName [ TypeArgs ] | "(" Expression ")" .
// Literal     = <BasicLiteral> | CompositeLit | FunctionLit .
// OperandName = identifier | QualifiedIdent .
type BasicOperand interface{}

type StringLiteral struct {
	Token token.Token
}

type IntegerLiteral struct {
	Value uint64
	Token token.Token
}

// <UnaryOperator> = "+" | "-" | "!" | "^" | "*" | "&" | "<-"

// <BinaryOperator> = <BooleanOperator> | <RelationOperator> | <AdditionOperator> | <MultiplicationOperator>
// <BooleanOperator> = "||" | "&&"
// <RelationOperator> = "==" | "!=" | "<" | "<=" | ">" | ">="
// <AdditionOperator> = "+" | "-" | "|" | "^"
// <MultiplicationOperator> = "*" | "/" | "%" | "<<" | ">>" | "&" | "&^"
// type BinaryOperator struct {
// 	Token token.Token
// }

// <Statement> = <BlockStatement> | <AssignStatement> | <DefineStatement> | <IfStatement> |
//
//	<DeferStatement> | <ExpressionStatement> | <ReturnStatement> | <MatchStatement> |
//	<LoopStatement> | <VarStatement>
type Statement interface{}

// <VarStatement> = <VarDeclaration> | <VarDefinition>
type VarStatement any

// <VarDeclaration> = "var" <Name> ":" <TypeSpecifier> ";"
//
// <Name> = <Identifier>
type VarDeclaration struct {
	Name Identifier
	Type TypeSpecifier
}

// <VarDeclaration> = "var" <Name> ":" <TypeSpecifier> "=" "dirty" ";"
//
// <Name> = <Identifier>
type VarDirty struct {
	Name Identifier
	Type TypeSpecifier
}

// <TypedAssign> = <Name> ":" <TypeSpecifier> "=" <Expression> ";"
//
// <Name> = <Identifier>
type TypedAssign struct {
	Name       Identifier
	Type       TypeSpecifier
	Expression Expression
}

// <ReturnStatement> = "return" <Expression> ";"
type ReturnStatement struct {
	Expression Expression
}

// <ForEver> = "for" <BlockStatement>
//
// for loop with no predicate condition
type ForEver struct {
	Body BlockStatement
}

// <While> = "for" <Expression> <BlockStatement>
//
// while loop with predicate condition
type While struct {
	Body BlockStatement

	Condition Expression
}

// <AssignStatement> = <SimpleAssignStatement> | <MultipleAssignStatement>
type AssignStatement interface{}

// <SimpleAssignStatement> = <AssignableExpression> <AssignOperator> <Expression>
type Assign struct {
	Target     Identifier
	Expression Expression
}

// <ShortAssign> = <Name> ":=" <Expression> ";"
//
// <Name> = <Identifier>
type ShortAssign struct {
	Name       Identifier
	Expression Expression
}

// <VarShortAssign> = "var" <Name> ":=" <Expression> ";"
type VarShortAssign struct {
	Name       Identifier
	Expression Expression
}

// <AddAssign> = <Name> "+=" <Expression> ";"
//
// <Name> = <Identifier>
type AddAssign struct {
	Target     Identifier
	Expression Expression
}

// <AssignableExpression> = <Identifier> | <SelectorExpression> | <IndexExpression>
type AssignableExpression interface{}

// <VarDefinition> = <VarDeclaration> "=" <Expression>
type VarDefinition struct {
	Declaration VarDeclaration
	Expression  Expression
}

type TopLevelVar struct {
	// <VarShortAssign> | <VarDirty> | <VarDeclaration> | <VarDefinition>
	Var any

	Public bool
}

type TopLevelConst struct {
	Const any

	Public bool
}

// <BlockStatement> = "{", <StatementList>, "}"
type BlockStatement struct {
	// starting position of block
	Pos source.Pos

	Statements []Statement
}

// <IfStatement> = <IfClause> [ <ElseClause> ]
type IfStatement struct {
	If   IfClause
	Else *ElseClause
}

// <IfClause> = "if", <Expression>, <BlockStatement>
type IfClause struct {
	Condition Expression
	Body      BlockStatement
}

// <ElseClause> = "else" <BlockStatement>
type ElseClause struct {
	Body BlockStatement
}

type ExpressionStatement struct {
	Expression Expression
}

// <ArrayTypeLiteral> = "[" <ArrayLengthSpecifier> "]" <ElementTypeSpecifier>
//
// <ArrayLengthSpecifier> = <Expression>
//
// <ElementTypeSpecifier> = <TypeSpecifier>
type ArrayTypeLiteral struct {
	Length Expression
	Spec   TypeSpecifier
}

// <PointerTypeLiteral> = "*" <BaseTypeSpecifier>
//
// <BaseTypeSpecifier> = <TypeSpecifier>
type PointerTypeLiteral struct {
	BaseType TypeSpecifier
}

// <SliceTypeLiteral> = "[]" <ElementTypeSpecifier>
//
// <ElementTypeSpecifier> = <TypeSpecifier>
type SliceTypeLiteral struct {
	ElementType TypeSpecifier
}

// <MapTypeLiteral> = "[" <KeyTypeSpecifier> "]" "=>" <ValueTypeSpecifier>
//
// <KeyTypeSpecifier> = <TypeSpecifier>
//
// <ValueTypeSpecifier> = <TypeSpecifier>
type MapTypeLiteral struct {
	KeyType   TypeSpecifier
	ValueType TypeSpecifier
}

// <SetTypeLiteral> = "[" <ElementTypeSpecifier> "]"
//
// <ElementTypeSpecifier> = <TypeSpecifier>
type SetTypeLiteral struct {
	ElementType TypeSpecifier
}

type QualifiedIdentifier struct {
	ModuleName Identifier
	Identifier Identifier
}
