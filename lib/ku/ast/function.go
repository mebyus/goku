package ast

// <FunctionDeclaration> = [ "pub" ] "fn" <Name> <Signature>
//
// <Name> = <Identifier>
//
// <Signature> = <FunctionSignature>
type FunctionDeclaration struct {
	Atrs      AtrBlock
	Signature FunctionSignature
	Name      Identifier
	Public    bool
}

// <FunctionDefinition> = <Declaration> <Body>
//
// <Declaration> = <FunctionDeclaration>
//
// <Body> = <BlockStatement>
type FunctionDefinition struct {
	Declaration FunctionDeclaration

	Body BlockStatement
}

// <FunctionSignature> = <Parameters> [ "=>" <Result> ]
type FunctionSignature struct {
	Parameters Tuple

	// Equals nil if function returns nothing
	Result TypeSpecifier
}

// <Tuple> = <EmptyTuple> | <NamedTuple> | <UnnamedTuple>
type Tuple any

// <FieldDefinition> = <Name> ":" <TypeSpecifier>
//
// <Name> = <Identifier>
type FieldDefinition struct {
	Name Identifier
	Type TypeSpecifier
}

// <EmptyTuple> = "(" ")"
type EmptyTuple struct{}

// <NamedTuple> = "(" <FieldDefinition> { "," <FieldDefinition> } [ "," ] ")"
type NamedTuple []FieldDefinition

// <UnnamedTuple> = "(" <TypeSpecifier> { "," <TypeSpecifier> } [ "," ] ")"
type UnnamedTuple []TypeSpecifier
