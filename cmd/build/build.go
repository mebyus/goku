package build

import (
	"fmt"
	"os"

	"codeberg.org/mebyus/goku/lib/ku/builder"
	"codeberg.org/mebyus/goku/lib/ku/ir/cer"
	"codeberg.org/mebyus/goku/lib/ku/source"
	"codeberg.org/mebyus/goku/lib/sys"
)

func Build(path, outname string) {
	bd := builder.New()
	err := bd.Build(path, outname)
	if err != nil {
		er, ok := err.(*cer.Error)
		if !ok {
			sys.Fatal(err)
		}

		if er.Span == nil {
			sys.Fatal(er)
		}

		fmt.Fprintf(os.Stderr, "error: %s\n\n", er.Error())
		source.Render(os.Stderr, er.Span, source.LineWindow{
			Before: 5,
			After:  5,
		})
		os.Exit(1)
	}
}
