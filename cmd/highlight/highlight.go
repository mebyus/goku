package highlight

import (
	"os"

	"codeberg.org/mebyus/goku/lib/ku/lexer"
	"codeberg.org/mebyus/goku/lib/ku/source"
	"codeberg.org/mebyus/goku/lib/ku/token"
	"codeberg.org/mebyus/goku/lib/sys"
)

func Highlight(filename string, tokidx int) {
	src, err := source.Load(filename)
	if err != nil {
		sys.Fatal(err)
	}
	lx := lexer.FromSource(src)

	var found token.Token
	i := 0
	for {
		tok := lx.Lex()

		if i == tokidx {
			found = tok
			break
		}
		i++

		if tok.IsEOF() {
			sys.Fatalf("unable to find token at index %d, stream contains only %d tokens", tokidx, i)
		}
	}

	err = source.Render(os.Stdout, found, source.LineWindow{
		Before: 5,
		After:  5,
	})
	if err != nil {
		sys.Fatal(err)
	}
}
