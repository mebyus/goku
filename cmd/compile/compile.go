package compile

import (
	"fmt"

	"codeberg.org/mebyus/goku/lib/ku/ast"
	"codeberg.org/mebyus/goku/lib/ku/ir"
	"codeberg.org/mebyus/goku/lib/ku/ir/symbol"
	"codeberg.org/mebyus/goku/lib/ku/parser"
	"codeberg.org/mebyus/goku/lib/sys"
)

func Compile(filename string) {
	source, err := parser.ParseFile(filename)
	if err != nil {
		sys.Fatal(err)
	}
	tree, err := ir.ConstructUnit("", 0, []*ast.UnitSourceCode{source}, nil)
	if err != nil {
		sys.Fatal(err)
	}

	for _, sym := range tree.Symbols {
		fmt.Printf("%s  %d\n", sym.Name, sym.Kind)
		if sym.Kind == symbol.Fn {
			for _, s := range sym.Def.(*ir.Fn).Body.Scope.Names {
				fmt.Printf("  %s  %d\n", s.Name, s.Kind)
			}
		}
	}
}
