package main

import (
	"fmt"
	"os"
	"strconv"

	"codeberg.org/mebyus/goku/cmd/build"
	"codeberg.org/mebyus/goku/cmd/compile"
	"codeberg.org/mebyus/goku/cmd/highlight"
	"codeberg.org/mebyus/goku/cmd/lex"
	"codeberg.org/mebyus/goku/cmd/parse"
	"codeberg.org/mebyus/goku/lib/sys"
)

func main() {
	var cmd string
	var args []string

	if len(os.Args) < 2 {
		cmd = "help"
	} else {
		cmd = os.Args[1]
		args = os.Args[2:]
	}

	switch cmd {
	case "lex":
		if len(args) == 0 {
			sys.Fatal("filename must be specified to invoke lex command")
		}
		filename := args[0]
		lex.Lex(filename)
	case "highlight":
		if len(args) == 0 {
			sys.Fatal("filename must be specified to invoke highlight command")
		}
		filename := args[0]
		if len(args) < 2 {
			sys.Fatal("token index must be specified to invoke highlight command")
		}
		s := args[1]
		idx, err := strconv.ParseUint(s, 10, 64)
		if err != nil {
			sys.Fatal(err)
		}
		highlight.Highlight(filename, int(idx))
	case "parse":
		if len(args) == 0 {
			sys.Fatal("filename must be specified to invoke parse command")
		}
		filename := args[0]
		parse.Parse(filename)
	case "compile":
		if len(args) == 0 {
			sys.Fatal("filename must be specified to invoke compile command")
		}
		filename := args[0]
		compile.Compile(filename)
	case "build":
		var path, outname string
		outname = "main.ku.gen.c"
		if len(args) == 0 {
			path = "."
		} else if len(args) == 1 {
			path = args[0]
		} else if args[0] == "-o" {
			outname = args[1]
			if len(args) == 3 {
				path = args[2]
			} else {
				path = "."
			}
		}
		build.Build(path, outname)
	case "help":
		fmt.Print(usage)
	default:
		sys.Fatal("unknown command: " + cmd)
	}
}

const usage = `ku is a command line tool for managing Ku source code

Usage:

	ku <command> [arguments]

Available commands:

	lex
	parse
	compile
	build
	help

`
