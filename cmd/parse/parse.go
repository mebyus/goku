package parse

import (
	"fmt"
	"time"

	"codeberg.org/mebyus/goku/lib/ku/parser"
	"codeberg.org/mebyus/goku/lib/sys"
)

func Parse(filename string) {
	start := time.Now()
	sourceCode, err := parser.ParseFile(filename)
	fmt.Println("elapsed:", time.Since(start))
	if err != nil {
		sys.Fatal(err)
	}
	fmt.Printf("%+#v\n", sourceCode)
}
