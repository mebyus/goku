package lex

import (
	"os"

	"codeberg.org/mebyus/goku/lib/ku/lexer"
	"codeberg.org/mebyus/goku/lib/sys"
)

func Lex(filename string) {
	lx, err := lexer.FromFile(filename)
	if err != nil {
		sys.Fatal(err)
	}
	err = lexer.List(os.Stdout, lx)
	if err != nil {
		sys.Fatal(err)
	}
}
